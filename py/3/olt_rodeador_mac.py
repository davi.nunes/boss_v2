#!/usr/bin/python3

import threading
import paramiko
import time
import sys
import os

class ssh:
    shell = None
    client = None
    transport = None
    last = ""
   

    def __init__(self, address, username, password):
        print("Connecting to", str(address) + ".")
        self.client = paramiko.client.SSHClient()
        self.client.set_missing_host_key_policy(paramiko.client.AutoAddPolicy())
        self.client.connect(address, username=username, password=password, look_for_keys=False)
        self.transport = paramiko.Transport((address, 22))
        self.transport.connect(username=username, password=password)

        thread = threading.Thread(target=self.process)
        thread.daemon = True
        thread.start()

    def closeConnection(self):
        if(self.client != None):
            self.client.close()
            self.transport.close()

    def openShell(self):
        self.shell = self.client.invoke_shell()

    def sendShell(self, command):
        self.last = self.last + "\r\n" + command + "\r\n"
        if(self.shell):
            self.shell.send(command + "\r\n")
        else:
            print("Shell not opened.")

    def ShellSpace(self):
        self.last = self.last + "\r\n \r\n"
        if(self.shell):
            self.shell.send(" ")
        else:
            print("Shell not opened.")

    def ShellAnswerY(self):
        self.shell.send("Y")

    def process(self):
        global connection
        while True:
            # Print data when available
            if self.shell != None and self.shell.recv_ready():
                alldata = self.shell.recv(1024)
                while self.shell.recv_ready():
                    alldata += self.shell.recv(1024)
                strdata = str(alldata, "utf8")
                strdata.replace('\r', '')
                self.last = self.last + strdata
                print(strdata, end = "")
                if(strdata.endswith("$ ")):
                    print("\n$ ", end = "")


sshUsername = "root"
sshPassword = "admin"
sshServer = "172.24.4.2"
enPassword = "admin"


connection = ssh(sshServer, sshUsername, sshPassword)
connection.openShell()

# connection.CommandEnable(enPassword)
# connection.WaitForPrompt()
time.sleep(0.1)
connection.sendShell("enable")
time.sleep(0.5)
connection.sendShell("config")
time.sleep(0.5)
connection.sendShell("show mac-address port epon 0/0/1 with-ont-location")
time.sleep(0.8)
connection.sendShell("show mac-address port epon 0/0/2 with-ont-location")
time.sleep(0.8)
connection.sendShell("show mac-address port epon 0/0/3 with-ont-location")
time.sleep(0.8)
connection.sendShell("show mac-address port epon 0/0/4 with-ont-location")
time.sleep(0.8)
connection.sendShell("show mac-address port epon 0/0/5 with-ont-location")
time.sleep(0.8)
connection.sendShell("show mac-address port epon 0/0/6 with-ont-location")
time.sleep(0.8)
connection.sendShell("show mac-address port epon 0/0/7 with-ont-location")
time.sleep(0.8)
connection.sendShell("show mac-address port epon 0/0/8 with-ont-location")
time.sleep(0.8)
connection.sendShell("exit")
time.sleep(0.8)
connection.sendShell("exit")
time.sleep(0.5)
connection.sendShell("exit")
time.sleep(0.5)