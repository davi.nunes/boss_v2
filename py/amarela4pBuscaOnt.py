#!/usr/bin/env python
#-------------------------------------
#by Jorge Luiz Taioque
#jorgeluiztaioque at gmail dot com
#www.networktips.com.br
#-------------------------------------
#backup OLTs and ONUs fiberhome
#Usage 
#./bk-olt-fiberhome.py IP_ADDRESS


import sys,pexpect
import getpass
import time
import sys


USER = 'levi'
PASSWORD = '131377'

HOST = "172.16.7.34"
mac = sys.argv[1]


child = pexpect.spawn ('telnet '+HOST) #option needs to be a list
time.sleep(0.2)
child.expect ('Login:')
child.sendline (USER)
child.expect('Password:')
child.sendline(PASSWORD)
time.sleep(0.2)
child.logfile_read = sys.stdout
time.sleep(0.2)
child.sendline ('terminal length 0')
child.expect('>')
child.sendline ('enable') #going to ENABLE configuration
child.expect('Password:')
child.sendline(PASSWORD)
child.expect('#')
child.sendline ('configure terminal')
child.expect('#')
child.sendline ('show mac address-table address '+mac)
child.expect('#')
child.sendline ('show onu opm-diag all')
child.expect('#')

child.sendline ('exit')
child.expect('>')
child.sendline ('exit')
