var repetir = 750;
var encerrar = 0;
var respawn = 4000;
var Temperatura = new Array();
var Ventoinha = new Array();
var Hora = new Array();

$(document).ready(function(){
	
	var contar = setInterval(function(){
		nat();
		$("#nat").show();
		encerrar++;
		if(encerrar > repetir){
			clearInterval(contar);
		}
	},respawn);
	
	$("#speedset").click(function(){
		var olt = $("#dados").attr("olt");
		var speed = $("#speed").val();
		var url = 'index.php?metodo=set&olt='+olt+'&speed='+speed;
		$.post(url, "", function(data) {
		   M.toast({html: data, classes: 'rounded'});
		});
	});
	
});

function nat() {
	var olt = $("#dados").attr("olt");
	var url = 'index.php?metodo=nat&olt='+olt;
	console.log(url);
		$.post(url, "", function(data) {
		   var dados = JSON.parse(data);
			console.log(dados);
			if(dados.temp == ""){
				dados.temp = 0;
			}
			$("#dados").attr("temp",dados.temp);
			$("#dados").attr("fan",dados.fan);
		});
		
}


Highcharts.chart('nat', {
  chart: {
    type: 'areaspline',
    animation: Highcharts.svg, // don't animate in old IE
    marginRight: 10,
    events: {
      load: function () {
        // set up the updating of the chart each second
        var series0 = this.series[0];
		var series1 = this.series[1];

        var ddd = setInterval(function () {
          var 	x = (new Date()).getTime(), // current time
				y = parseInt($("#dados").attr("temp"));
			Temperatura.push(y);
			Hora.push(x);
          series0.addPoint([x, y], true, true);
		  if(encerrar > repetir){
			  clearInterval(ddd);
		  }
        }, respawn);
		var uuu = setInterval(function () {
			var x = (new Date()).getTime(), // current time
				y = parseInt($("#dados").attr("fan"));
			Ventoinha.push(y);
			series1.addPoint([x, y], true, true);
		  if(encerrar > repetir){
			  clearInterval(uuu);
		  }
        }, respawn);

      }
    }
  },

  time: {
    useUTC: false
  },

  title: {
    text: 'Temperatura da OLT'
  },

  accessibility: {
    announceNewData: {
      enabled: false,
      minAnnounceInterval: 15000,
      announcementFormatter: function (allSeries, newSeries, newPoint) {
        if (newPoint) {
          return 'New point added. Value: ' + newPoint.y;
        }
        return false;
      }
    }
  },

  xAxis: {
    type: 'datetime',
    tickPixelInterval: 150
  },

  yAxis: {
    title: {
      text: 'Graus'
    },
    plotLines: [{
      value: 0,
      width: 1,
      color: '#808080'
    }]
  },

  tooltip: {
    headerFormat: '<b>{series.name}</b><br/>',
    pointFormat: '{point.x:%Y-%m-%d %H:%M:%S}<br/>{point.y:.2f}'
  },

  legend: {
    enabled: true
  },

  exporting: {
    enabled: false
  },

  series: [{
    name: 'Temperatura',
    data: (function () {
      // generate an array of random data
      var data = [],
        time = (new Date()).getTime(),
        i;

      for (i = -(repetir-2); i <= 0; i += 1) {
        data.push({
          x: time + i * 1000,
          y: 0
        });
      }
      return data;
    }())
  },{
    name: 'Ventoinha',
    data: (function () {
      // generate an array of random data
      var data = [],
        time = (new Date()).getTime(),
        i;

      for (i = -(repetir-2); i <= 0; i += 1) {
        data.push({
          x: time + i * 1000,
          y: 0
        });
      }
      return data;
	  
    }())
  }]
  
});