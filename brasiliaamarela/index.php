<head>

  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Relatório 8 pon Brasilia Amarela </title>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.2/css/bulma.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
  <link rel="stylesheet" href="amarela.css">
  <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
</head>
<div class="">
<div class="row">
<div class="col s12 m12 l6" id="8pons">
	<center>
	<p><b>Aguarde, Coletando os dados da olt de 8 portas...</b></p>
	<div class="lds-ripple">
		<div></div>
		<div></div>
	</div>
	</center>
</div>

<div class="col s12 m12 l6" id="4pons">
	<center>
	<p><b>Aguarde, Coletando os dados da olt de 4 portas...</b></p>
	<div class="lds-ripple">
		<div></div>
		<div></div>
	</div>
	</center>
</div>
</div>
</div>

<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

<script src="amarela.js?<?php echo time(); ?>"></script>