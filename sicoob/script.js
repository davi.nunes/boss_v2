$(document).ready(function(){
	
	const ontem = moment().subtract(1, 'days').format('YYYY-MM-DD');
	document.getElementById('dataSelecionada').value = ontem;
	
	var resposta = $("#resposta");
	
	M.Modal.init(document.querySelectorAll('.modal'));
	
	$('select').formSelect();
	
	var timeoutId; // Variável para armazenar o ID do timeout

	$("#nomeCpfCnpj").on('input', function() { // Neste campo consulto exclusivamente no IXC
	  clearTimeout(timeoutId); // Limpa o timeout anterior, se houver
	  resposta.html('');
	  $(".input-loader").show(); // Mostra o ícone de carregamento

	  var keyword = $(this).val();

	  // Define um novo timeout de 1 segundos
	  timeoutId = setTimeout(function() {
		// Remove a barra de progresso após o tempo do timeout
		$(".input-loader").hide(); // Esconde o ícone de carregamento após o tempo do timeout
		M.toast({html: "Aguarde um instante", classes: 'rounded'});

		// Função de sucesso para atualizar a tabela
		function successCallback(data) {
		  M.toast({html: "Dados encontrados", classes: 'rounded'});
		  resposta.html(criarTabela(data));
		}

		// Função de erro para lidar com falhas na requisição
		function errorCallback(jqXHR, textStatus, errorThrown) {
		  console.log("Erro na solicitação AJAX: " + textStatus);
		  console.log("Detalhes do erro: " + errorThrown);
		}

		// Fazer a requisição AJAX com a função getClienteData
		getClienteData(keyword, successCallback, errorCallback);
	  }, 1000); // 3000 milissegundos = 3 segundos
	  
	});

	
	$("#via_sicoob").keyup(function() {

	  clearTimeout(timeoutId); // Limpa o timeout anterior, se houver
	  $(".input-loader").show(); // Mostra o ícone de carregamento

	  var keyword = $(this).val();
	  
	  if ((isCPF(keyword) || isCPFFormatted(keyword)) && isCPFValid(keyword)) {
		  M.toast({html: "Você digitou um CPF?", classes: 'rounded'});
			if(isCPFFormatted(keyword)){
				keyword = cleanCPF(keyword);
				M.toast({html: "Ajustado Formato aceito Sicoob!", classes: 'rounded'});
			}
	  }else if ((isCNPJ(keyword) || isCNPJFormatted(keyword)) && isCNPJValid(keyword)) {
		  M.toast({html: "Você digitou um CNPJ?", classes: 'rounded'});
			if(isCNPJFormatted(keyword)){
				keyword = cleanCNPJ(keyword);
				M.toast({html: "Ajustado Formato aceito Sicoob!", classes: 'rounded'});
			}
	  }

	  // Define um novo timeout de 3 segundos
	  timeoutId = setTimeout(function() {
		  $(".input-loader").hide(); // Esconde o ícone de carregamento
		  if(keyword.length === 47){
			  M.toast({html: "Buscando por linha digitável!", classes: 'rounded'});
			  viaSicoobLinhaDigitavel(keyword);
		  }else if(isCPFValid(keyword) || isCNPJValid(keyword)){
			  M.toast({html: "Buscando por CPF ou CNPJ! "+keyword, classes: 'rounded'});
			  viaSicoobCpfCnpj(keyword);
		  }else{
			  M.toast({html: "Tentando busca por nosso numero", classes: 'rounded'});
			  viaSicoobNossoNumero(keyword)
		  }
		
	  }, 1000); // 3000 milissegundos = 3 segundos
	});
	
	
});

function getClienteData(keyword, successCallback, errorCallback) { //Pega dodos do IXC
  if ((isCPF(keyword) || isCPFFormatted(keyword)) && isCPFValid(keyword)) {
    M.toast({html: "Você digitou um CPF?", classes: 'rounded'});
    if(!isCPFFormatted(keyword)){
      keyword = formatarCPF(keyword);
      M.toast({html: "Ajustado Formato aceito pelo IXC", classes: 'rounded'});
    }
  }else if((isCNPJ(keyword) || isCNPJFormatted(keyword)) && isCNPJValid(keyword)) {
    M.toast({html: "Você digitou um CNPJ?", classes: 'rounded'});
    if(!isCNPJFormatted(keyword)){
      keyword = formatarCNPJ(keyword);
      M.toast({html: "Ajustado Formato aceito pelo IXC: "+keyword, classes: 'rounded'});
    }
  }

  url = 'metodo.php?cliente=' + keyword;
  // console.log(url);

  $.getJSON(url, "", function(data) {
    successCallback(data);
  }).fail(function(jqXHR, textStatus, errorThrown) {
    errorCallback(jqXHR, textStatus, errorThrown);
  });
}

function criarTabela(jsonData) { // Dados do IXC
  // Verifica se há dados
  if (jsonData.length === 0) {
    return "<p>Nenhum dado encontrado.</p>";
  }

  // Cria a tabela
  var table = "<table>";
  table += "<thead><tr><th>Razão</th><th>CNPJ/CPF</th><th>Nosso Número</th><th>Link PagPix</th><th>Valor</th><th>Observação</th><th>Data de Vencimento</th><th>Status IXC</th><th>Linha Digitável</th></tr></thead>";
  table += "<tbody>";

  // Itera sobre os dados e adiciona as linhas na tabela
  for (var i = 0; i < jsonData.length; i++) {
    var row = jsonData[i];
	
	let classe = getClassByStatusIXC(row.status);
	let nossonum = row.nosso_numero ? "<td><a class='btn getNossoNumero'>" + row.nosso_numero + "</a></td>" : "<td></td>";
	const idtitulo = `<a class="pagpix btn" href="https://acessodf.net/pagpix/?titulo=${row.id}">${row.id}</a>`;
	
    table += "<tr class='"+classe+"'>";
    table += "<td>" + row.razao + "</td>";
    table += "<td>" + row.cnpj_cpf + "</td>";
    table += nossonum;
    table += "<td>" + idtitulo + "</td>";
    table += "<td>" + row.valor + "</td>";
    table += "<td>" + row.obs + "</td>";
    table += "<td>" + moment(row.data_vencimento).format('DD/MM/YYYY') + "</td>";
    table += "<td>" + transcreverStatusIXC(row.status) + "</td>";
    table += "<td><a class='getLinhaDigitavel'>" + row.linha_digitavel + "</a></td>";
    table += "</tr>";
  }

  table += "</tbody>";
  table += "</table>";

  return table;
}

function criarDetalheIxc(jsonData) { // Dados do IXC
  // Verifica se há dados
  if (jsonData.length === 0) {
    return "<p>Nenhum dado encontrado.</p>";
  }

  // Cria a tabela
  var table = "<table>";
  table += "<thead><tr><th>Razão</th><th>CNPJ/CPF</th><th>Nosso Número</th><th>ID</th><th>Valor</th><th>Observação</th><th>Data de Vencimento</th><th>Status IXC</th><th>Linha Digitável</th></tr></thead>";
  table += "<tbody>";

  // Itera sobre os dados e adiciona as linhas na tabela
  for (var i = 0; i < jsonData.length; i++) {
    var row = jsonData[i];
    table += "<tr>";
    table += "<td>" + row.razao + "</td>";
    table += "<td>" + row.cnpj_cpf + "</td>";
    table += "<td><a class=' '>" + row.nosso_numero + "</a></td>";
    table += "<td>" + row.id + "</td>";
    table += "<td>" + row.valor + "</td>";
    table += "<td>" + row.obs + "</td>";
    table += "<td>" + moment(row.data_vencimento).format('DD/MM/YYYY') + "</td>";
    table += "<td>" + row.status + "</td>";
    table += "<td><a class=''>" + row.linha_digitavel + "</a></td>";
    table += "</tr>";
  }

  table += "</tbody>";
  table += "</table>";

  return table;
}

function criarCard(jsonData) {
  var cardHTML = '';
  
  // Extrai os dados do JSON
  var situacaoBoleto = jsonData.resultado.situacaoBoleto;
  var historico = jsonData.resultado.listaHistorico;

  // Cria o fragmento de HTML do card
  cardHTML += '<div class="card indigo lighten-5">';
  cardHTML += '  <div class="card-content">';
  cardHTML += '    <span class="card-title">Situação do Boleto no Sicoob</span>';
  cardHTML += '    <p>' + situacaoBoleto + '</p>';
  cardHTML += '  </div>';
  cardHTML += '  <div class="card-content">';
  cardHTML += '    <span class="card-title">Histórico do Boleto</span>';
  cardHTML += '    <ul class="collection">';
  
  for (var i = 0; i < historico.length; i++) {
    var historicoItem = historico[i];
    var dataHistorico = moment(historicoItem.dataHistorico).format('DD/MM/YYYY'); // Formata a data no formato brasileiro
    cardHTML += '      <li class="collection-item">';
    cardHTML += '        <span class="data-historico">' + dataHistorico + '</span>';
    cardHTML += '        <span class="descricao-historico">' + historicoItem.descricaoHistorico + '</span>';
    cardHTML += '      </li>';
  }
  
  cardHTML += '    </ul>';
  
  cardHTML += addActionsFragment(jsonData.resultado.nossoNumero);
  
  cardHTML += '  </div>';
  cardHTML += '</div>';

  return cardHTML;
}

function criarCardCompleto(jsonData) {
  var cardHTML = '';
  
  // Extrai os dados do JSON
  var situacaoBoleto = jsonData.resultado.situacaoBoleto;
  var historico = jsonData.resultado.listaHistorico;

  // Cria o fragmento de HTML do card
  cardHTML += '<div class="card indigo lighten-5">';
  cardHTML += '  <div class="card-content">';
  cardHTML += '    <span class="card-title">Situação do Boleto no Sicoob</span>';
  cardHTML += '    <p>' + situacaoBoleto + '</p><br>';
  cardHTML += '    <p>Titular: ' + jsonData.resultado.pagador.nome + '</p>';
  cardHTML += '    <p>CPF/CNPJ: ' + jsonData.resultado.pagador.numeroCpfCnpj + '</p>';
  cardHTML += '    <p>Nosso Numero: ' + jsonData.resultado.nossoNumero + '</p>';
  cardHTML += '    <p>Valor: ' + jsonData.resultado.valor + '</p>';
  cardHTML += '  </div>';
  cardHTML += '  <div class="card-content">';
  cardHTML += '    <span class="card-title">Histórico do Boleto</span>';
  cardHTML += '    <ul class="collection">';
  
  for (var i = 0; i < historico.length; i++) {
    var historicoItem = historico[i];
    var dataHistorico = moment(historicoItem.dataHistorico).format('DD/MM/YYYY'); // Formata a data no formato brasileiro
    cardHTML += '      <li class="collection-item">';
    cardHTML += '        <span class="data-historico">' + dataHistorico + '</span>';
    cardHTML += '        <span class="descricao-historico">' + historicoItem.descricaoHistorico + '</span>';
    cardHTML += '      </li>';
  }
  
  cardHTML += '    </ul>';
  cardHTML += '    <span class="card-title">Dados no IXC</span>';
  cardHTML += '    <span id="span"></span>';
  
  
  cardHTML += addActionsFragment(jsonData.resultado.nossoNumero);
  
  
  cardHTML += '  </div>';
  cardHTML += '</div>';

  return cardHTML;
}

function viaSicoobLinhaDigitavel(arg){
	var resposta = $("#resposta");
	url = 'metodo.php?linhaDigitavel=' + arg;
	$.ajax({
	  url: url,
	  dataType: "json",
	  success: function(data) {
		  resposta.html(criarCardCompleto(data));
		  var onIxc = getIxcNossoNumero(data.resultado.nossoNumero);
		  console.log(onIxc);
	  },
	  error: function(jqXHR, textStatus, errorThrown) {
		console.log("Erro na solicitação AJAX: " + textStatus);
		console.log("Detalhes do erro: " + errorThrown);
		linhaLinha.after("<tr><td colspan='90'>Não há dados</td></tr>");
	  }
	});
}

function viaSicoobNossoNumero(arg){
	var resposta = $("#resposta");
	url = 'metodo.php?nossoNumero=' + arg;
	$.ajax({
	  url: url,
	  dataType: "json",
	  success: function(data) {
		  resposta.html(criarCardCompleto(data));
		  var onIxc = getIxcNossoNumero(data.resultado.nossoNumero);
		  console.log(onIxc);
	  },
	  error: function(jqXHR, textStatus, errorThrown) {
		console.log("Erro na solicitação AJAX: " + textStatus);
		console.log("Detalhes do erro: " + errorThrown);
		linhaLinha.after("<tr><td colspan='90'>Não há dados</td></tr>");
	  }
	});
}

function viaSicoobCpfCnpj(arg){
	var resposta = $("#resposta");
	url = 'metodo.php?cpfCnpj=' + arg;
	$.ajax({
	  url: url,
	  dataType: "json",
	  success: function(data) {
		  resposta.html(arrayParaTabelaCpfCnpj(data.resultado));

	  },
	  error: function(jqXHR, textStatus, errorThrown) {
		console.log("Erro na solicitação AJAX: " + textStatus);
		console.log("Detalhes do erro: " + errorThrown);
		linhaLinha.after("<tr><td colspan='90'>Não há dados</td></tr>");
	  }
	});
}

function getIxcNossoNumero(numero){
	url = 'metodo.php?cliente=' + numero;
	// Realizar a solicitação GET para obter os dados desejados
	$.getJSON(url, "", function(data) {

	  // Após obter a resposta do GET, atualizar a tabela
	  // Exemplo:
	  // console.log(data);
		// return(data);
		$("#span").html(criarDetalheIxc(data));

	});
}

function addActionsFragment(nossoNumero){
	let fragmento = '';
	
	fragmento += '<div>';
		fragmento += '<a class="btn setPix" nossoNumero="' + nossoNumero + '">Ativar Pix</a> ';
		fragmento += '<a class="btn segundaVia" nossoNumero="' + nossoNumero + '">Baixar boleto modelo Sicoob</a> ';
	fragmento += '</div>';
	
	return fragmento;
}

function downloadArquivo(data) {
    var arquivoBase64 = data.resultado.arquivo;
    var nomeArquivo = data.resultado.nomeArquivo;

	// Decodifica o arquivo base64 para um array de bytes
	var byteCharacters = atob(arquivoBase64);
	var byteArrays = [];

	for (var offset = 0; offset < byteCharacters.length; offset += 1024) {
		var slice = byteCharacters.slice(offset, offset + 1024);

		var byteNumbers = new Array(slice.length);
		for (var i = 0; i < slice.length; i++) {
			byteNumbers[i] = slice.charCodeAt(i);
		}

		var byteArray = new Uint8Array(byteNumbers);
		byteArrays.push(byteArray);
	}

	var blob = new Blob(byteArrays, { type: "application/octet-stream" });

	// Cria um URL temporário para o blob
	var url = URL.createObjectURL(blob);

	// Cria um link no DOM e simula o clique nele para iniciar o download
	var link = document.createElement("a");
	link.style.display = "none";
	link.href = url;
	link.download = nomeArquivo;

	document.body.appendChild(link);
	link.click();

	// Remove o link do DOM após o download
	document.body.removeChild(link);

}

// Função para descompactar o arquivo ZIP e obter o objeto JSON
function descompactarJSON(conteudoZipBase64, callback) {
  // Cria um objeto JSZip
  let zip = new JSZip();

  // Decodifica o conteúdo do arquivo ZIP em base64
  let conteudoZip = atob(conteudoZipBase64);

  // Carrega o conteúdo do arquivo ZIP
  zip.loadAsync(conteudoZip, { base64: false })
    .then(function(zip) {
      // Extrai o conteúdo do arquivo JSON (assumindo que há apenas um arquivo no ZIP)
      let nomeArquivoJSON = Object.keys(zip.files)[0];
      return zip.file(nomeArquivoJSON).async("string");
    })
    .then(function(conteudoJSON) {
      // Converte o conteúdo do JSON em objeto
      let objetoJSON = JSON.parse(conteudoJSON);
      
      // Chama a função de retorno (callback) com o objeto JSON descompactado
      callback(objetoJSON);
    })
    .catch(function(error) {
      console.log("Erro ao descompactar o arquivo ZIP: ", error);
      callback(null); // Chama o callback com null em caso de erro
    });
}

function arrayParaTabelaTipo5(data) { // Liquidações
  // Verifica se o array está vazio ou nulo
  if (!data || data.length === 0) {
    return "<p>Nenhum dado disponível</p>";
  }

  // Cria a tabela HTML
  let tabelaHTML = `
    <table class="striped responsive-table">
      <thead>
        <tr class='teal'>
          <th>#</th>
          <th>Nome</th>
          <th>Status IXC</th>
          <th>Nosso Número</th>
          <th>Data de Vencimento do Título</th>
          <th>Valor do Título</th>
          <th>Data de Liquidação</th>
          <th>Data de Previsão de Crédito</th>
          <th>Valor de Desconto</th>
          <th>Valor de Mora</th>
          <th>Valor Líquido</th>
        </tr>
      </thead>
      <tbody>
  `;
	
	let total  = 0;
  // Preenche os dados do array na tabela
	data.forEach((item, index) => {
	  
	  total += item.valorLiquido;
	  // let pagador = getClienteData(idBoleto, getPagador, errorCallback);
    // Verifica se há juros de mora (valorMora maior que zero) e adiciona a classe de cor correspondente
    let corMora = item.valorMora > 0 ? 'blue lighten-4' : '';

    // Verifica se há desconto (valorDesconto maior que zero) e adiciona a classe de cor correspondente
    let corDesconto = item.valorDesconto > 0 ? 'green lighten-4' : '';

    // Define a classe de cor para a linha inteira
    let corLinha = corMora || corDesconto ? 'class="' + corMora + ' ' + corDesconto + '"' : '';
	let cellId = item.numeroTitulo.toString().slice(0, -1);
    tabelaHTML += `
      <tr ${corLinha}>
        <td>${index + 1}</td>
        <td id="num${cellId}"></td>
        <td id="stat${cellId}"></td>
        <td>${item.numeroTitulo}</td>
        <td>${formatarData(item.dataVencimentoTitulo)}</td>
        <td>${formataValor(item.valorTitulo)}</td>
        <td>${formatarData(item.dataLiquidacao)}</td>
        <td>${formatarData(item.dataPrevisaoCredito)}</td>
        <td>${formataValor(item.valorDesconto)}</td>
        <td>${formataValor(item.valorMora)}</td>
        <td>${formataValor(item.valorLiquido)}</td>
      </tr>
    `;
	});
	
	// console.log(valorFormatado); // Output: R$ 11.638,98
	// Neste exemplo, a função toLocaleString formata o valor com o estilo "currency" (moeda) e a moeda definida como "BRL" (Real brasileiro). O resultado será "R$ 11.638,98", que é o formato desejado.


	tabelaHTML += `<tr class='teal'> 
				<td colspan='10'>Total</td>
				<td >${formataValor(total)}</td>
				</tr>`;
	
  // Fecha a tabela
  tabelaHTML += `
      </tbody>
    </table>
  `;

  return tabelaHTML;
}

function converterParaLista(obj) {
  let listaHTML = '<ul>';
  for (let prop in obj) {
    if (typeof obj[prop] === 'object' && obj[prop] !== null) {
      listaHTML += `<li>${prop}: ${converterParaLista(obj[prop])}</li>`;
    } else {
      listaHTML += `<li>${prop}: ${obj[prop]}</li>`;
    }
  }
  listaHTML += '</ul>';
  return listaHTML;
}

function formataValor(valor){
	valor = valor.toLocaleString('pt-BR', {style: 'currency',currency: 'BRL',});
	return valor;
}
  
function arrayParaTabela(data) { // Tabela Desconhecida
  // Verifica se o array está vazio, nulo ou não é um objeto válido
  if (!data || typeof data !== 'object' || Object.keys(data).length === 0) {
    return "<p>Nenhum dado disponível</p>";
  }

  // Cria a tabela HTML
  let tabelaHTML = "<table class='highlight responsive-table'><thead><tr>";

  // Obtém os campos disponíveis no primeiro objeto do array
  let campos = Object.keys(data[0]);

  // Adiciona os cabeçalhos da tabela
  campos.forEach((campo) => {
    tabelaHTML += `<th>${campo}</th>`;
  });

  tabelaHTML += "</tr></thead><tbody>";

  // Preenche os dados do array na tabela
  data.forEach((item) => {
    tabelaHTML += "<tr>";
    campos.forEach((campo) => {
      // Verifica se o valor é um objeto e itera sobre ele para gerar uma lista dentro do td
      if (typeof item[campo] === 'object' && item[campo] !== null) {
        tabelaHTML += `<td>${converterParaLista(item[campo])}</td>`;
      } else if (campo.includes("data") && item[campo]) {
        tabelaHTML += `<td>${formatarData(item[campo])}</td>`;
      } else {
        tabelaHTML += `<td>${item[campo]}</td>`;
      }
    });
    tabelaHTML += "</tr>";
  });

  // Fecha a tabela
  tabelaHTML += "</tbody></table>";

  return tabelaHTML;
}

function formatarTabelaPixRecebidos(pixData) {
  let tableHtml = `
    <table class="table">
      <thead>
        <tr>
          <th>End To End ID</th>
          <th>Valor</th>
          <th>Horário</th>
          <th>Nome do Pagador</th>
          <th>Anotação do Pagador</th>
          <th>Devoluções</th>
        </tr>
      </thead>
      <tbody>
  `;

  pixData.forEach(entry => {
    const devolucoesHtml = entry.devolucoes.map(dev => `
      <div class="devolucao-entry">
        <span>${dev.id}</span>
        <span class="devolucao-status">${dev.status}</span>
      </div>
    `).join('');

    const endToEndIdOrTxid = entry.endToEndId ? entry.endToEndId : entry.txid;

    const infoPagadorHtml = entry.infoPagador ? `
      <div class="spoiler">
        <span class="spoiler-title"></span>
        <div class="spoiler-content">${entry.infoPagador}</div>
      </div>
    ` : '';
	
	const cpfcnpj = entry.pagador.cnpj ? entry.pagador.cnpj : entry.pagador.cpf;

    tableHtml += `
      <tr>
        <td>${endToEndIdOrTxid}</td>
        <td>${entry.valor}</td>
        <td>${new Date(entry.horario).toLocaleString()}</td>
        <td cpf="${cpfcnpj}">${entry.nomePagador} - ${cpfcnpj}</td>
        <td >${infoPagadorHtml}</td>
        <td class="devolucoes">${devolucoesHtml}</td>
      </tr>
    `;
  });

  tableHtml += `
      </tbody>
    </table>
  `;

  return tableHtml;
}

function arrayParaTabelaCpfCnpj(data) {
  // Verifica se o array está vazio ou nulo
  if (!data || data.length === 0) {
    return "<p>Nenhum dado disponível</p>";
  }

  // Cria a tabela HTML
  let tabelaHTML = "<table><thead><tr>";

  // Obtém as chaves das propriedades para criar o cabeçalho da tabela
  const keys = [
    "situacaoBoleto",
    "pagador",
    // "numeroContrato",
    // "modalidade",
    "nossoNumero",
    // "seuNumero",
    "dataEmissao",
    // "codigoBarras",
    "valor",
    "dataVencimento",
    // "valorMulta",
    // "tipoJurosMora",
    // "dataJurosMora",
    // "tipoMulta",
    // "dataMulta",
    "linhaDigitavel",
  ];

  // Preenche o cabeçalho da tabela com as chaves formatadas
  keys.forEach((chave) => {
    tabelaHTML += `<th>${formatarTextoParaCabecalho(chave)}</th>`;
  });

  // Fecha o cabeçalho da tabela
  tabelaHTML += "</tr></thead><tbody>";

  // Preenche os dados do array na tabela
  data.forEach((item) => {
    tabelaHTML += "<tr>";
    keys.forEach((chave) => {
      let valor = item[chave];

      // Formata o valor da coluna "valor" como moeda
      if (chave === "valor" || chave === "valorMulta") {
        valor = valor.toFixed(2);
      }

      // Formata as datas no formato desejado
      if (chave === "dataEmissao" || chave === "dataVencimento" || chave === "dataJurosMora" || chave === "dataMulta") {
        valor = formatarData(valor);
      }

      // Formata o campo "pagador" como nome ou CPF/CNPJ
      if (chave === "pagador") {
        valor = formatarPagador(valor);
      }

      tabelaHTML += `<td>${valor}</td>`;
    });
    tabelaHTML += "</tr>";
  });

  // Fecha a tabela
  tabelaHTML += "</tbody></table>";

  return tabelaHTML;
}

function formatarData(data) {
  // Converte a data para o formato desejado (por exemplo, "dd/mm/aaaa")
  const dataObj = new Date(data);
  const dia = dataObj.getDate().toString().padStart(2, '0');
  const mes = (dataObj.getMonth() + 1).toString().padStart(2, '0');
  const ano = dataObj.getFullYear();
  return `${dia}/${mes}/${ano}`;
}

function arrayParaListaDeCards(data) {
  // Verifica se o array está vazio ou nulo
  if (!data || data.length === 0) {
    return "<p>Nenhum dado disponível</p>";
  }

  // Cria a lista de cards HTML usando a estrutura do Materialize
  let listaHTML = "<div class='row'>";

  // Preenche os dados do array nos cards
  data.forEach((item) => {
    // Verifica se há juros de mora (valorMora maior que zero) e adiciona a classe de cor correspondente
    let corMora = item.valorMora > 0 ? 'blue lighten-4' : '';

    // Verifica se há desconto (valorDesconto maior que zero) e adiciona a classe de cor correspondente
    let corDesconto = item.valorDesconto > 0 ? 'green lighten-4' : '';

    listaHTML += `
      <div class='col s12 m6 l4'>
        <div class='card ${corMora} ${corDesconto}'>
          <div class='card-content'>
            <p><strong>Sigla do Movimento:</strong> ${item.siglaMovimento}</p>
            <p><strong>Seu Número:</strong> ${item.seuNumero}</p>
            <p><strong>Nosso Número:</strong> ${item.numeroTitulo}</p>
            <p><strong>Data de Vencimento do Título:</strong> ${formatarData(item.dataVencimentoTitulo)}</p>
            <p><strong>Valor do Título:</strong> R$ ${item.valorTitulo.toFixed(2)}</p>
            <p><strong>Data de Liquidação:</strong> ${formatarData(item.dataLiquidacao)}</p>
            <p><strong>Data de Previsão de Crédito:</strong> ${formatarData(item.dataPrevisaoCredito)}</p>
            <p><strong>Número do Banco Recebedor:</strong> ${item.numeroBancoRecebedor}</p>
            <p><strong>Número da Agência Recebedora:</strong> ${item.numeroAgenciaRecebedora}</p>
            <p><strong>ID Tipo Operação Financeira:</strong> ${item.idTipoOpFinanceira}</p>
            <p><strong>Valor de Desconto:</strong> R$ ${item.valorDesconto.toFixed(2)}</p>
            <p><strong>Valor de Mora:</strong> R$ ${item.valorMora.toFixed(2)}</p>
            <p><strong>Valor Líquido:</strong> R$ ${item.valorLiquido.toFixed(2)}</p>
          </div>
        </div>
      </div>
    `;
  });

  // Fecha a lista de cards
  listaHTML += "</div>";

  return listaHTML;
}

function formatarTextoParaCabecalho(texto) {
  // Converte camelCase para palavras separadas por espaço
  return texto.replace(/([A-Z])/g, " $1").trim();
}

function formatarPagador(pagador) {
  if (pagador && pagador.nome) {
    return pagador.nome.trim();
  } else if (pagador && pagador.numeroCpfCnpj) {
    const cpfCnpj = pagador.numeroCpfCnpj.trim();
    if (cpfCnpj.length === 11) {
      return formatarCPF(cpfCnpj);
    } else if (cpfCnpj.length === 14) {
      return formatarCNPJ(cpfCnpj);
    }
  }
  return "-";
}

function formatarCNPJ(cnpj) {
  return cnpj.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/, "$1.$2.$3/$4-$5");
}

function formatarCPF(cpf) {
  return cpf.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, "$1.$2.$3-$4");
}

function isCPF(cpf) {
  cpf = cpf.replace(/[^\d]+/g, ''); // Remove caracteres não numéricos

  if (cpf.length !== 11) return false;

  // Verifica se todos os dígitos são iguais (caso especial, mas inválido)
  for (let i = 1; i < 11; i++) {
    if (cpf.charAt(i) !== cpf.charAt(0)) {
      break;
    }
    if (i === 10) return false;
  }

  let sum = 0;
  let remainder;

  for (let i = 1; i <= 9; i++) {
    sum += parseInt(cpf.charAt(i - 1)) * (11 - i);
  }

  remainder = (sum * 10) % 11;

  if (remainder === 10 || remainder === 11) remainder = 0;

  if (remainder !== parseInt(cpf.charAt(9))) return false;

  sum = 0;

  for (let i = 1; i <= 10; i++) {
    sum += parseInt(cpf.charAt(i - 1)) * (12 - i);
  }

  remainder = (sum * 10) % 11;

  if (remainder === 10 || remainder === 11) remainder = 0;

  if (remainder !== parseInt(cpf.charAt(10))) return false;

  return true;
}

function isCNPJ(cnpj) {
  cnpj = cnpj.replace(/[^\d]+/g, ''); // Remove caracteres não numéricos

  if (cnpj.length !== 14) return false;

  // Verifica se todos os dígitos são iguais (caso especial, mas inválido)
  for (let i = 1; i < 14; i++) {
    if (cnpj.charAt(i) !== cnpj.charAt(0)) {
      break;
    }
    if (i === 13) return false;
  }

  let size = 12;
  let numbers = cnpj.substring(0, size);
  const digits = cnpj.substring(size);
  let sum = 0;
  let pos = size - 7;

  for (let i = size; i >= 1; i--) {
    sum += numbers.charAt(size - i) * pos--;
    if (pos < 2) pos = 9;
  }

  let result = sum % 11 < 2 ? 0 : 11 - (sum % 11);
  if (result != digits.charAt(0)) return false;

  size = size + 1;
  numbers = cnpj.substring(0, size);
  sum = 0;
  pos = size - 7;

  for (let i = size; i >= 1; i--) {
    sum += numbers.charAt(size - i) * pos--;
    if (pos < 2) pos = 9;
  }

  result = sum % 11 < 2 ? 0 : 11 - (sum % 11);
  if (result != digits.charAt(1)) return false;

  return true;
}

function isCPFValid(cpf) {
  // Remove caracteres não numéricos do CPF
  const cleanedCPF = cpf.replace(/\D/g, '');
  
  // Verifica se o CPF tem 11 dígitos
  if (cleanedCPF.length !== 11) {
    return false;
  }

  // Verifica se todos os dígitos do CPF são iguais (CPF inválido)
  if (/^(\d)\1{10}$/.test(cleanedCPF)) {
    return false;
  }

  // Cálculo do primeiro dígito verificador
  let sum = 0;
  for (let i = 0; i < 9; i++) {
    sum += parseInt(cleanedCPF.charAt(i)) * (10 - i);
  }
  let remainder = sum % 11;
  let digit1 = remainder < 2 ? 0 : 11 - remainder;

  // Cálculo do segundo dígito verificador
  sum = 0;
  for (let i = 0; i < 10; i++) {
    sum += parseInt(cleanedCPF.charAt(i)) * (11 - i);
  }
  remainder = sum % 11;
  let digit2 = remainder < 2 ? 0 : 11 - remainder;

  // Verifica se os dígitos calculados são iguais aos dígitos do CPF original
  if (digit1 !== parseInt(cleanedCPF.charAt(9)) || digit2 !== parseInt(cleanedCPF.charAt(10))) {
    return false;
  }

  return true;
}

function isCNPJValid(cnpj) {
  // Remove caracteres não numéricos do CNPJ
  const cleanedCNPJ = cnpj.replace(/\D/g, '');

  // Verifica se o CNPJ tem 14 dígitos
  if (cleanedCNPJ.length !== 14) {
    return false;
  }

  // Verifica se todos os dígitos do CNPJ são iguais (CNPJ inválido)
  if (/^(\d)\1{13}$/.test(cleanedCNPJ)) {
    return false;
  }

  // Cálculo do primeiro dígito verificador
  let sum = 0;
  let factor = 5;
  for (let i = 0; i < 12; i++) {
    sum += parseInt(cleanedCNPJ.charAt(i)) * factor;
    factor = factor === 2 ? 9 : factor - 1;
  }
  let remainder = sum % 11;
  let digit1 = remainder < 2 ? 0 : 11 - remainder;

  // Cálculo do segundo dígito verificador
  sum = 0;
  factor = 6;
  for (let i = 0; i < 13; i++) {
    sum += parseInt(cleanedCNPJ.charAt(i)) * factor;
    factor = factor === 2 ? 9 : factor - 1;
  }
  remainder = sum % 11;
  let digit2 = remainder < 2 ? 0 : 11 - remainder;

  // Verifica se os dígitos calculados são iguais aos dígitos do CNPJ original
  if (digit1 !== parseInt(cleanedCNPJ.charAt(12)) || digit2 !== parseInt(cleanedCNPJ.charAt(13))) {
    return false;
  }

  return true;
}

function isCPFFormatted(cpf) {
  const pattern = /^[0-9]{3}\.[0-9]{3}\.[0-9]{3}\-[0-9]{2}$/;
  return pattern.test(cpf);
}

function isCNPJFormatted(cnpj) {
  const pattern = /^\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}$/;
  return pattern.test(cnpj);
}

function cleanCPF(cpf) {
  return cpf.replace(/[^\d]/g, '');
}

function cleanCNPJ(cnpj) {
  return cnpj.replace(/[^\d]/g, '');
}

function fazerConsultaMovimento(idMovimento, tentativaAtual, maxTentativas) {
  if (tentativaAtual > maxTentativas) {
    M.toast({html: 'Limite máximo de tentativas atingido. Ainda não há registros disponíveis.', classes: 'rounded'});
	$('.temporizador-texto').html('<h3>Limite máximo de tentativas atingido. Ainda não há registros disponíveis!</h3>');
	$('.temporizador').hide();
	tentativaAtual = 0;
    return;
  }
	$('.temporizador').show();
	$('.temporizador-texto').html("Aguarde...");
	
  // Realizar a solicitação GET para obter os dados desejados
  let url = 'metodo.php?consultaMovimento=' + idMovimento;
  $.ajax({
    url: url,
    dataType: "json",
    success: function(data) {
      // console.log(data);
      if (data.resultado && data.resultado.quantidadeTotalRegistros > 0) {
        let mensagem = 'Existem ' + data.resultado.quantidadeTotalRegistros + ' registros no arquivo ' + data.resultado.idArquivos[0];
        M.toast({html: mensagem, classes: 'rounded'});
        $("#getMovimento").attr("movimento", idMovimento);
        $("#getMovimento").attr("arquivo", data.resultado.idArquivos[0]);
		fecharModalTemporizador();
        setTimeout(function() {
          $("#getMovimento").click();
        }, 500);
      } else if (data.mensagens && data.mensagens[0].codigo === "5004") {
        // Ainda em processamento, aguardar e fazer outra tentativa
		// M.toast({html: "Ainda em processamento, aguarde um pouco. Tentativa " + tentativaAtual + " de " + maxTentativas, classes: 'rounded'});
		
//////////////// abrirModalTemporizador();



		atualizarPreenchimento(tentativaAtual/maxTentativas*100);
		
        setTimeout(function() {
          fazerConsultaMovimento(idMovimento, tentativaAtual + 1, maxTentativas);
        }, 2000); // Intervalo de 2 segundos entre as tentativas
      } else {
        M.toast({html: 'Solicitação não retornou nenhum registro.', classes: 'rounded'});

      }
    },
    error: function(jqXHR, textStatus, errorThrown) {
      console.log("Erro na solicitação AJAX: " + textStatus);
      console.log("Detalhes do erro: " + errorThrown);
    }
  });
}

// Função para abrir o modal
function abrirModalTemporizador() {
  const modalInstance = M.Modal.getInstance(document.getElementById('modalTemporizador'));
  modalInstance.open();
}

// Função para fechar o modal
function fecharModalTemporizador() {
  const modalInstance = M.Modal.getInstance(document.getElementById('modalTemporizador'));
  modalInstance.close();
}

function spin(element){
	$(element).html('<div class="preloader-wrapper small active">      <div class="spinner-layer spinner-blue">        <div class="circle-clipper left">          <div class="circle"></div>        </div><div class="gap-patch">          <div class="circle"></div>        </div><div class="circle-clipper right">          <div class="circle"></div>        </div>      </div>      <div class="spinner-layer spinner-red">        <div class="circle-clipper left">          <div class="circle"></div>        </div><div class="gap-patch">          <div class="circle"></div>        </div><div class="circle-clipper right">          <div class="circle"></div>        </div>      </div>      <div class="spinner-layer spinner-yellow">        <div class="circle-clipper left">          <div class="circle"></div>        </div><div class="gap-patch">          <div class="circle"></div>        </div><div class="circle-clipper right">          <div class="circle"></div>        </div>      </div>      <div class="spinner-layer spinner-green">        <div class="circle-clipper left">          <div class="circle"></div>        </div><div class="gap-patch">          <div class="circle"></div>        </div><div class="circle-clipper right">          <div class="circle"></div>        </div>      </div>    </div>');
}

// Função para atualizar o preenchimento do círculo temporizador
function atualizarPreenchimento(porcentagem) {
  const temporizador = document.querySelector(".temporizador");
  porcentagem = Math.max(0, Math.min(100, parseFloat(porcentagem)));
  temporizador.style.setProperty("--porcentagem", `${porcentagem}%`);
  temporizador.style.animation = "preenchimento 1s linear forwards";
}

function getPagadoresIxc(data) { //Insere dados dos pagadores na movimentação de Liquidação
  const listaNumerosTituloJSON = JSON.stringify(data);
  const dados = {
    listaNumerosTitulo: listaNumerosTituloJSON
  };

  $.ajax({
    url: 'metodo.php?clientes=true',
    type: 'POST',
    data: dados,
    dataType: 'json',
    success: function (response) {
      // Armazena os dados em formato JSON no elemento escondido
      // $('#dadosJson').text(JSON.stringify(response));
      
      // Disparar o evento personalizado quando os dados forem atualizados
      // $('#dadosJson').click();
	  
      // Processar a resposta para obter informações dos pagadores
		  for (const numeroTitulo in response) {
			const pagador = response[numeroTitulo][0];
			const razaoSocial = pagador.razao;
			const stat = pagador.status;
			const id = pagador.id;
			const celula = "#num" + id;
			const statusixc = "#stat" + id;
			// Tente atualizar o HTML do elemento diretamente com um valor de exemplo
			$(celula).html(razaoSocial);
			$(statusixc).html(stat);
			if(stat !== 'R'){
				// console.log(statusixc);
				$(statusixc).parent().addClass("red"); // Adiciona a classe "red" à célula pai
			}
		  }
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log('Erro na requisição AJAX:', textStatus, errorThrown);
    }
  });
};

function transcreverStatusIXC(status) {
  switch (status) {
    case 'A':
      return 'Em Aberto';
    case 'R':
      return 'Recebido';
    case 'C':
      return 'Cancelado';
    case 'P':
      return 'Parcial';
    default:
      return 'Status Desconhecido';
  }
}
  // Função para copiar texto para a área de transferência
function copyToClipboard(text) {
	var input = $("<input>").val(text);
	$("body").append(input);
	input.select();
	document.execCommand("copy");
	input.remove();
}

function getClassByStatusIXC(status) {
  switch (status) {
    case 'A':
      return 'blue lighten-5';
    case 'R':
      return 'green lighten-5';
    case 'C':
      return 'grey lighten-2';
    case 'P':
      return 'teal lighten-5';
    default:
      return '';
  }
}

// Evento para verificar o valor do campo ao digitar
$("#nomeCpfCnpj").on('input', function() {
  const valor = $(this).val();
  if (valor.length === 11) {
    if (isCPF(valor)) {
      console.log("CPF válido");
    } else {
      console.log("CPF inválido");
    }
  } else if (valor.length === 14) {
    if (isCNPJ(valor)) {
      console.log("CNPJ válido");
    } else {
      console.log("CNPJ inválido");
    }
  }
});

$(document).on('click', '#dadosJson', function() { // gambiarra depreciada, mas anotado
	console.log("Recebeu clique");
  // Lê os dados atualizados em formato JSON
  const jsonData = $(this).text();

  // Processa os dados e atualiza a tabela conforme necessário
  const data = JSON.parse(jsonData);
  console.log(data);
  for (const numeroTitulo in data) {
    const pagador = data[numeroTitulo][0];
    const razaoSocial = pagador.razao;
    const id = pagador.id;
    const celula = "num" + id;
	  console.log(celula);
	  console.log($('#'+celula));

    // Atualiza o HTML da célula com a razão social
    $($('#'+celula)).html(razaoSocial);
  }
});

$(document).on('click', '.getNossoNumero', function() {
  let nossaLinha = $(this).closest("tr");
  let nossoNumero = $(this).text();
  let rowIndex = nossaLinha.index();
		$(this).removeClass("getNossoNumero");
  
	url = 'metodo.php?nossoNumero=' + nossoNumero;
	// Realizar a solicitação GET para obter os dados desejados
	$.getJSON(url, "", function(data) {

	  // Após obter a resposta do GET, atualizar a tabela
	  // Exemplo:
		nossaLinha.after("<tr><td colspan='90'>"+criarCard(data)+"</td></tr>");

	});
});

$(document).on('click', '.getLinhaDigitavel', function() {
  let linhaLinha = $(this).closest("tr");
  let rowIndex = linhaLinha.index();
  let linhaDigitavel = $(this).text();
		  $(this).removeClass("getLinhaDigitavel");

  // Realizar a solicitação GET para obter os dados desejados
  url = 'metodo.php?linhaDigitavel=' + linhaDigitavel;
  $.ajax({
	  url: url,
	  dataType: "json",
	  success: function(data) {
		  linhaLinha.after("<tr><td colspan='90'>"+criarCard(data)+"</td></tr>");
	  },
	  error: function(jqXHR, textStatus, errorThrown) {
		console.log("Erro na solicitação AJAX: " + textStatus);
		console.log("Detalhes do erro: " + errorThrown);
		linhaLinha.after("<tr><td colspan='90'>Não há dados</td></tr>");
	  }
	});
});

$(document).on('click', '.segundaVia', function() {
  let nossoNumero = $(this).attr("nossoNumero");
  
  // Realizar a solicitação GET para obter os dados desejados
  url = 'metodo.php?segundaVia=' + nossoNumero;
  $.ajax({
	  url: url,
	  dataType: "json",
	  success: function(data) {
		  if (data.resultado.pdfBoleto) {
			// Criar uma nova janela e definir seu conteúdo como o PDF
			// let pdfWindow = window.open();
			// pdfWindow.document.write('<embed width="100%" height="100%" type="application/pdf" src="data:application/pdf;base64,' + data.resultado.pdfBoleto + '"></embed>');

		  
		  // Criar o conteúdo da modal com o PDF
			let modalContent = '<div id="pdfModal" class="modal ">';
			modalContent += '<div class="modal-content">';
			modalContent += '<h4>PDF</h4>';
			modalContent += '<embed width="100%" height="500" type="application/pdf" src="data:application/pdf;base64,' + data.resultado.pdfBoleto + '"></embed>';
			modalContent += '</div>';
			modalContent += '<div class="modal-footer">';
			modalContent += '<a id="btnMaximizar" href="#!" class="waves-effect waves-green btn-flat">Maximizar</a>';
			modalContent += '<a href="#!" class="modal-close waves-effect waves-green btn-flat">Fechar</a>';
			modalContent += '</div>';
			modalContent += '</div>';

			// Verificar se a modal já existe, e se sim, removê-la antes de criar uma nova
			$('#pdfModal').remove();

			// Adicionar a modal ao corpo da página
			$('body').append(modalContent);

			// Inicializar a modal
			let pdfModal = $('#pdfModal');
			pdfModal.modal();
			pdfModal.modal('open');
		  } else {
			console.log("Resposta do servidor não contém o PDF.");
		  }
	  },
	  error: function(jqXHR, textStatus, errorThrown) {
		console.log("Erro na solicitação AJAX: " + textStatus);
		console.log("Detalhes do erro: " + errorThrown);
		M.toast({ html: 'PDF indisponível', classes: 'rounded red' });
	  }
	});
});

$(document).on('click', '.setPix', function() { // Ativa pix no boleto [não está habilitado na carteira]
  let nossoNumero = $(this).attr("nossoNumero");
  
  // Realizar a solicitação GET para obter os dados desejados
  url = 'metodo.php?setPix=' + nossoNumero;
  $.ajax({
	  url: url,
	  dataType: "json",
	  success: function(data) {
		  // console.log(data);
		  let classe = "";
		  if(data.resultado[0].status.codigo == '200'){
			  classe += 'green rounded';
		  }else{
			  classe += 'red rounded';
		  }
		  let mensagem = data.resultado[0].status.codigo + ': ' +data.resultado[0].status.mensagem
		  M.toast({html: mensagem, classes: classe});
	  },
	  error: function(jqXHR, textStatus, errorThrown) {
		console.log("Erro na solicitação AJAX: " + textStatus);
		console.log("Detalhes do erro: " + errorThrown);
	  }
	});
});

$(document).on('click', '#getExtrato', function() { // Extrato CC [Não está habilitado no app criado]
    
  // Realizar a solicitação GET para obter os dados desejados
	const dataObj = new Date($("#dataSelecionada").val());
	const mes = dataObj.getMonth() + 1; // Lembrando que os meses são indexados a partir de 0, por isso adicionamos 1
	const ano = dataObj.getFullYear();
	
	// console.log("Mês:", mes); // Saída: Mês: 7 (julho)
	// console.log("Ano:", ano); // Saída: Ano: 2023
  
  const url = `metodo.php?extrato=true&mes=${mes}&ano=${ano}`;
  
  $.ajax({
	  url: url,
	  dataType: "json",
	  success: function(data) {
		  console.log(data);

	  },
	  error: function(jqXHR, textStatus, errorThrown) {
		console.log("Erro na solicitação AJAX: " + textStatus);
		console.log("Detalhes do erro: " + errorThrown);
	  }
	});
});

$(document).on('click', '#btnMaximizar', function() { // Aumenta a largura do modal do boleto
	$('#pdfModal').toggleClass('maximizado');
	if ($('#pdfModal').hasClass('maximizado')) {
      $('#btnMaximizar').text('Restaurar');
    } else {
      $('#btnMaximizar').text('Maximizar');
    }
});

$(document).on('click', '#movimentoPix', function() { // Obtém extrato Pix
    
  // Realizar a solicitação GET para obter os dados desejados
  let inicio = $("#dataSelecionada").val();
  let fim = $("#dataSelecionada").val();
  
  var resposta = $("#resposta");
  
  let tabelaHTML;
  
  url = 'metodo.php?extratoPix=sim&inicio='+inicio+'&fim='+fim;
  
  $.ajax({
	  url: url,
	  dataType: "json",
	  success: function(data) {
		console.log(data);
		// tabelaHTML = arrayParaTabela(data.pix);
		tabelaHTML = formatarTabelaPixRecebidos(data.pix);
		resposta.html(tabelaHTML);
	  },
	  error: function(jqXHR, textStatus, errorThrown) {
		console.log("Erro na solicitação AJAX: " + textStatus);
		console.log("Detalhes do erro: " + errorThrown);
	  }
	});
});

$(document).on('click', '#movimento', function() { // Fase 01 - Solicitar consulta de movimentação

	spin(this);
    
  // Realizar a solicitação GET para obter os dados desejados
  let dataSelecionada = $("#dataSelecionada").val();
  let filtroStatus = $("#filtroStatus").val();
  
  url = 'metodo.php?movimento=sim&dataSelecionada='+dataSelecionada+'&filtroStatus='+filtroStatus;
  
  $.ajax({
	  url: url,
	  dataType: "json",
	  success: function(data) {
		  // console.log(data);
		  // console.log(data);
		  let codigo = data.resultado.codigoSolicitacao;
		  let mensagem = data.resultado.mensagem + ': ' +data.resultado.codigoSolicitacao;
		  M.toast({html: mensagem, classes: 'rounded'});
		  $("#consultaMovimento").attr("movimento",codigo);
		  setTimeout(function() {
			  $("#consultaMovimento").click();
			}, 750);
	  },
	  error: function(jqXHR, textStatus, errorThrown) {
		console.log("Erro na solicitação AJAX: " + textStatus);
		console.log("Detalhes do erro: " + errorThrown);
	  }
	});
});

$(document).on('click', '#consultaMovimento', function() { // Fase 02 - Verificar disponibilidade do processamento
	let idMovimento = $(this).attr("movimento");
  fazerConsultaMovimento(idMovimento, 1, 30); // Faz até 30 tentativas
});

$(document).on('click', '#getMovimento', function() { // Fase 03 - Obter a movimentação
	
	let idMovimento = $(this).attr("movimento");
	let idArquivo = $(this).attr("arquivo");
	
	var resposta = $("#resposta");
	var filtroStatus = $("#filtroStatus").val();
	
	let tabelaHTML;
    
  // Realizar a solicitação GET para obter os dados desejados
  url = 'metodo.php?downloadMovimento=' + idMovimento + '&idArquivo=' + idArquivo;
  $.ajax({
	  url: url,
	  dataType: "json",
	  success: function(data) {
		  // console.log(data);
		  // console.log(data);
		  // downloadArquivo(data);
		  // Verifica se a resposta contém o arquivo ZIP
		  if (data.resultado.arquivo && data.resultado.extensao === ".zip") {
			// Chama a função descompactarJSON com o conteúdo do arquivo ZIP em base64
			descompactarJSON(data.resultado.arquivo, function(objetoJSON) {
				// reseta o botão
				$("#movimento").html("Consultar movimentação do dia");
			  // Verifica se o objeto JSON foi obtido com sucesso
			  if (objetoJSON !== null) {
				// console.log("Objeto JSON descompactado:", objetoJSON);
				// let tabelaHTML = arrayParaListaDeCards(objetoJSON);
				console.log(filtroStatus);
				if(filtroStatus === '5'){
					tabelaHTML = arrayParaTabelaTipo5(objetoJSON);
					resposta.html(tabelaHTML);
					const listaNumeroTitulo = objetoJSON.map(item => {
						const numeroTitulo = item.numeroTitulo.toString();
						return numeroTitulo.slice(0, -1); // Retorna a substring sem o último dígito
					  });
					getPagadoresIxc(listaNumeroTitulo);
				}else{
					tabelaHTML = arrayParaTabela(objetoJSON);
					resposta.html(tabelaHTML);
				}
				// Chame outra função aqui para trabalhar com o JSON, se necessário
			  } else {
				console.log("Erro ao obter o objeto JSON descompactado.");
			  }
			});
		  }
			  // let mensagem = 'Existem ' + data.resultado.quantidadeTotalRegistros + ' registros no arquivo ' +data.resultado.idArquivos[0];
			  // M.toast({html: mensagem, classes: 'rounded'});
	  },
	  error: function(jqXHR, textStatus, errorThrown) {
		console.log("Erro na solicitação AJAX: " + textStatus);
		console.log("Detalhes do erro: " + errorThrown);
	  }
	});
});

$(document).on('click', '.spoiler-title', function() {
    $(this).siblings('.spoiler-content').toggle();
});

$(document).on("click", ".pagpix",function(event) {

	const href = $(event.target).attr("href");
    
      copyToClipboard(href);
      M.toast({html: 'Copiado para área de transferência: '+href, classes: 'rounded'});
    event.preventDefault();
});
  

