<?php //Sicoob
	$certificado = "acesso_com_servicos_de_telecomunicacoes_e_suprime30063355000190.pfx";
	$certs = "/var/www/includes/nfe/certs";
	$certs = "../../../includes/nfe/certs";
	$passwordCertificado = '123456';
	
	/* Geração de TOKEN se seção */
	
	function getToken(){ //Gera um token para ustilizar as demais endpoints
		$url = 'https://auth.sicoob.com.br/auth/realms/cooperado/protocol/openid-connect/token';
		
		$ch = curl_init($url); // Inicializa uma nova sessão cURL
		
		// Define as opções da solicitação
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'Content-Type: application/x-www-form-urlencoded'
		]);
		global $passwordCertificado;

		// Define o certificado PFX
		curl_setopt($ch, CURLOPT_SSLCERT, "/var/www/ilunne/boss/sicoob/certificado.pem");
		curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $passwordCertificado);
		
		// Define os parâmetros no payload
		$data = [
			'client_id' => '95841f2c-228e-4171-a77d-dc94bf68de2f',
			'grant_type' => 'client_credentials',
			'scope' => 'cob.read cob.write cobv.write cobv.read lotecobv.write lotecobv.read pix.write pix.read webhook.read webhook.write payloadlocation.write payloadlocation.read cobranca_boletos_consultar cobranca_boletos_incluir cobranca_boletos_pagador cobranca_boletos_segunda_via cobranca_boletos_descontos cobranca_boletos_abatimentos cobranca_boletos_valor_nominal cobranca_boletos_seu_numero cobranca_boletos_especie_documento cobranca_boletos_baixa cobranca_boletos_rateio_credito cobranca_pagadores cobranca_boletos_negativacoes_incluir cobranca_boletos_negativacoes_alterar cobranca_boletos_negativacoes_baixar cobranca_boletos_protestos_incluir cobranca_boletos_protestos_alterar cobranca_boletos_protestos_desistir cobranca_boletos_solicitacao_movimentacao_incluir cobranca_boletos_solicitacao_movimentacao_consultar cobranca_boletos_solicitacao_movimentacao_download cobranca_boletos_prorrogacoes_data_vencimento cobranca_boletos_prorrogacoes_data_limite_pagamento cobranca_boletos_encargos_multas cobranca_boletos_encargos_juros_mora cobranca_boletos_pix',
		];
		$dataString = http_build_query($data);
		// var_dump($dataString);

		// Define o corpo da solicitação
		curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

		// Executa a solicitação e obtém a resposta
		$response = curl_exec($ch);

		// Verifica se ocorreu algum erro na solicitação
		if(curl_errno($ch)) {
			echo 'Erro na requisição: ' . curl_error($ch);
		}

		// Decodifica a resposta JSON
		$responseData = json_decode($response, true);

		// Extrai o token da resposta
		$token = $responseData['access_token'];

		return $token;
		
	}

	/* Metodos para Boleto */
	
	function getBoletosCpf($token, $cpf) { //Recebe uma lista de Boletos informando um CPF/CNPJ
		$url = 'https://api.sicoob.com.br/cobranca-bancaria/v2/boletos/pagadores/' . $cpf;

		// Define os parâmetros no payload
		$data = [
			'numeroContrato' => '340529',
		];
		$url .= '?' . http_build_query($data);
		
		// echo $url;

		$ch = curl_init($url); // Inicializa uma nova sessão cURL

		// Define as opções da solicitação
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'Content-Type: application/json',
			'Accept: application/json',
			'client_id: 95841f2c-228e-4171-a77d-dc94bf68de2f',
			'Authorization: Bearer ' . $token
		]);

		// Define o certificado PFX
		curl_setopt($ch, CURLOPT_SSLCERT, "/var/www/ilunne/boss/sicoob/certificado.pem");
		curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $pfxPassword);
		$pfxPassword = '123456';

		// Executa a solicitação e obtém a resposta
		$response = curl_exec($ch);

		// Verifica se ocorreu algum erro na solicitação
		if (curl_errno($ch)) {
			return 'Erro na requisição: ' . curl_error($ch);
		} else {
			return $response;
		}

		curl_close($ch);
	}
	
	function getBoletoIndividual($token, $atributo, $valor) { //Obtém detalhes individuais de um boleto baseado no nossoNumero ou linhaDigitavel
		$url = 'https://api.sicoob.com.br/cobranca-bancaria/v2/boletos';

		// Define os parâmetros no payload
		$data = [
			'numeroContrato' => '340529',
			'modalidade' => '1',
			"$atributo" => "$valor",
		];
		$url .= '?' . http_build_query($data);
		
		// echo $url;

		$ch = curl_init($url); // Inicializa uma nova sessão cURL

		// Define as opções da solicitação
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'Content-Type: application/json',
			'client_id: 95841f2c-228e-4171-a77d-dc94bf68de2f',
			'Accept: application/json',
			'Authorization: Bearer ' . $token
		]);

		// Define o certificado PFX
		curl_setopt($ch, CURLOPT_SSLCERT, "/var/www/ilunne/boss/sicoob/certificado.pem");
		curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $pfxPassword);
		$pfxPassword = '123456';

		// Executa a solicitação e obtém a resposta
		$response = curl_exec($ch);

		// Verifica se ocorreu algum erro na solicitação
		if (curl_errno($ch)) {
			return 'Erro na requisição: ' . curl_error($ch);
		} else {
			return $response;
		}

		curl_close($ch);
	}	
	
	function postSolicitaMovimento($token, $dataSelecionada, $filtroStatus) { // 1 - Cria uma solicitação de Movimentação
		
		$url = 'https://api.sicoob.com.br/cobranca-bancaria/v2/boletos/solicitacoes/movimentacao';
		global $passwordCertificado;
		

		// Define os parâmetros no payload
		$data = [
			'numeroContrato' => '340529',
			'tipoMovimento' => $filtroStatus,
			"dataInicial" => $dataSelecionada . "T00:00:00-03:00",
			"dataFinal" => $dataSelecionada . "T00:00:00-03:00",
		];
		
		// Converter os dados para o formato JSON
		$jsonData = json_encode($data);

		$ch = curl_init($url); // Inicializa uma nova sessão cURL

		// Define as opções da solicitação
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData); // Dados no formato JSON
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'Content-Type: application/json',
			'client_id: 95841f2c-228e-4171-a77d-dc94bf68de2f',
			'Accept: application/json',
			'Authorization: Bearer ' . $token
		]);

		// Define o certificado PFX
		curl_setopt($ch, CURLOPT_SSLCERT, "/var/www/ilunne/boss/sicoob/certificado.pem");
		curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $pfxPassword);
		$pfxPassword = $passwordCertificado;

		// Executa a solicitação e obtém a resposta
		$response = curl_exec($ch);

		// Verifica se ocorreu algum erro na solicitação
		if (curl_errno($ch)) {
			return 'Erro na requisição: ' . curl_error($ch);
		} else {
			return $response;
		}

		curl_close($ch);
	}	
	
	function getConsultaStatusMovimento($token, $movimento) { // 2 - Verifica se o processamento de uma movimentação Já terminou
		$url = 'https://api.sicoob.com.br/cobranca-bancaria/v2/boletos/solicitacoes/movimentacao';
		global $passwordCertificado;

		// Define os parâmetros no payload
		$data = [
			'numeroContrato' => '340529',
			"codigoSolicitacao" => "$movimento",
		];
		$url .= '?' . http_build_query($data);
		
		// echo $url;

		$ch = curl_init($url); // Inicializa uma nova sessão cURL

		// Define as opções da solicitação
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'Content-Type: application/json',
			'client_id: 95841f2c-228e-4171-a77d-dc94bf68de2f',
			'Accept: application/json',
			'Authorization: Bearer ' . $token
		]);

		// Define o certificado PFX
		curl_setopt($ch, CURLOPT_SSLCERT, "/var/www/ilunne/boss/sicoob/certificado.pem");
		curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $pfxPassword);
		$pfxPassword = $passwordCertificado;

		// Executa a solicitação e obtém a resposta
		$response = curl_exec($ch);

		// Verifica se ocorreu algum erro na solicitação
		if (curl_errno($ch)) {
			return 'Erro na requisição: ' . curl_error($ch);
		} else {
			return $response;
		}

		curl_close($ch);
	}	
	
	function getDownloadMovimento($token, $movimento, $idArquivo) { // 3 - Recebe um zip com um json do extrato de movimentação
		$url = 'https://api.sicoob.com.br/cobranca-bancaria/v2/boletos/movimentacao-download';
		global $passwordCertificado;

		// Define os parâmetros no payload
		$data = [
			'numeroContrato' => '340529',
			"codigoSolicitacao" => "$movimento",
			"idArquivo" => "$idArquivo",
		];
		$url .= '?' . http_build_query($data);
		
		// echo $url;

		$ch = curl_init($url); // Inicializa uma nova sessão cURL

		// Define as opções da solicitação
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'Content-Type: application/json',
			'client_id: 95841f2c-228e-4171-a77d-dc94bf68de2f',
			'Accept: application/json',
			'Authorization: Bearer ' . $token
		]);

		// Define o certificado PFX
		curl_setopt($ch, CURLOPT_SSLCERT, "/var/www/ilunne/boss/sicoob/certificado.pem");
		curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $pfxPassword);
		$pfxPassword = $passwordCertificado;

		// Executa a solicitação e obtém a resposta
		$response = curl_exec($ch);

		// Verifica se ocorreu algum erro na solicitação
		if (curl_errno($ch)) {
			return 'Erro na requisição: ' . curl_error($ch);
		} else {
			return $response;
		}

		curl_close($ch);
	}
	
	function getSegundaViaPDF($token, $atributo, $valor) { // Obtém o arquivo do Boleto em PDF no formato Base64
		$url = 'https://api.sicoob.com.br/cobranca-bancaria/v2/boletos/segunda-via';

		// Define os parâmetros no payload
		$data = [
			'numeroContrato' => '340529',
			'modalidade' => '1',
			"$atributo" => "$valor",
			"gerarPdf" => "true",
		];
		$url .= '?' . http_build_query($data);
		
		// echo $url;

		$ch = curl_init($url); // Inicializa uma nova sessão cURL

		// Define as opções da solicitação
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'Content-Type: application/json',
			'client_id: 95841f2c-228e-4171-a77d-dc94bf68de2f',
			'Accept: application/json',
			'Authorization: Bearer ' . $token
		]);

		// Define o certificado PFX
		curl_setopt($ch, CURLOPT_SSLCERT, "/var/www/ilunne/boss/sicoob/certificado.pem");
		curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $pfxPassword);
		$pfxPassword = '123456';

		// Executa a solicitação e obtém a resposta
		$response = curl_exec($ch);

		// Verifica se ocorreu algum erro na solicitação
		if (curl_errno($ch)) {
			return 'Erro na requisição: ' . curl_error($ch);
		} else {
			return $response;
		}

		curl_close($ch);
	}	

	function postCriaBoleto($token) { // Teste - trocar os dados de acesso depois
		
		$url = 'https://api.sicoob.com.br/cobranca-bancaria/v2/boletos';
		// $url = 'https://sandbox.sicoob.com.br/sicoob/sandbox/cobranca-bancaria/v2/boletos';
		
		global $passwordCertificado;
		

		// Define os parâmetros no payload
		$jsonData = '[
			{
			  "numeroContrato": "340529",
			  "modalidade": 1,
			  "numeroContaCorrente": 164631,
			  "especieDocumento": "DM",
			  "dataEmissao": "2023-07-24T00:00:00-03:00",
			  "seuNumero": "doc",
			  "identificacaoEmissaoBoleto": 1,
			  "identificacaoDistribuicaoBoleto": 1,
			  "valor": 5,
			  "dataVencimento": "2023-07-30T00:00:00-03:00",
			  "tipoDesconto": 0,
			  "valorPrimeiroDesconto": 1,
			  "tipoMulta": 0,
			  "tipoJurosMora": 3,
			  "numeroParcela": 1,
			  "pagador": {
				  "numeroCpfCnpj": "01691128104",
				  "nome": "Davi Nunes de França",
				  "endereco": "QI 24 1/13 E104",
				  "bairro": "Taguatinga Norte",
				  "cidade": "Brasília",
				  "cep": "72135240",
				  "uf": "DF",
				  "email": [
					"davi.nunes@gmail.com"
				  ]
				}
			}
		]';


		// $ch = curl_init($url); // Descomentar essa Linha!!

		// Define as opções da solicitação
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData); // Dados no formato JSON
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'Content-Type: application/json',
			'client_id: 95841f2c-228e-4171-a77d-dc94bf68de2f',
			'Accept: application/json',
			'Authorization: Bearer ' . $token
		]);

		// Define o certificado PFX
		curl_setopt($ch, CURLOPT_SSLCERT, "/var/www/ilunne/boss/sicoob/certificado.pem");
		curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $pfxPassword);
		$pfxPassword = $passwordCertificado;

		// Executa a solicitação e obtém a resposta
		$response = curl_exec($ch);

		// Verifica se ocorreu algum erro na solicitação
		if (curl_errno($ch)) {
			return 'Erro na requisição: ' . curl_error($ch);
		} else {
			return $response;
		}

		curl_close($ch);		
	}
	
	function setPix($token, $atributo, $valor){ // Ativa a função Pix em um Boleto
		$url = 'https://api.sicoob.com.br/cobranca-bancaria/v2/boletos/pix';
		$nossoNunero = $valor;
		global $passwordCertificado;

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'PATCH',
		  CURLOPT_POSTFIELDS =>'[
		  {
			"numeroContrato": "340529",
			"modalidade": 1,
			"nossoNumero": '.$nossoNunero.',
			"utilizarPix": true
		  }
		]',
		  CURLOPT_HTTPHEADER => array(
			'Content-Type: application/json',
			'client_id: 95841f2c-228e-4171-a77d-dc94bf68de2f',
			'Accept: application/json',
			'Authorization: Bearer ' . $token
		  ),
		));
		
		// Define o certificado PFX
		curl_setopt($curl, CURLOPT_SSLCERT, "/var/www/ilunne/boss/sicoob/certificado.pem");
		curl_setopt($curl, CURLOPT_SSLCERTPASSWD, $passwordCertificado);

		$response = curl_exec($curl);

		curl_close($curl);
		echo $response;

	}	
	
	function patchBaixaBoleto($token, $nossoNunero){ // Cancela um boleto no banco
		$url = 'https://api.sicoob.com.br/cobranca-bancaria/v2/boletos/baixa';
		
		global $passwordCertificado;
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'PATCH',
		  CURLOPT_POSTFIELDS =>'[
		  {
			"numeroContrato": "340529",
			"modalidade": 1,
			"nossoNumero": '.$nossoNunero.'
		  }
		]',
		  CURLOPT_HTTPHEADER => array(
			'Content-Type: application/json',
			'client_id: 95841f2c-228e-4171-a77d-dc94bf68de2f',
			'Accept: application/json',
			'Authorization: Bearer ' . $token
		  ),
		));
		
		// Define o certificado PFX
		curl_setopt($curl, CURLOPT_SSLCERT, "/var/www/ilunne/boss/sicoob/certificado.pem");
		curl_setopt($curl, CURLOPT_SSLCERTPASSWD, $passwordCertificado);

		$response = curl_exec($curl);

		curl_close($curl);
		echo $response;

	}

	/* Metodos para PIX */


	function pixImediato($token,$data) { // Cria uma cobrança Imediata
		
		$url = 'https://api.sicoob.com.br/pix/api/v2/cob';
		
		global $passwordCertificado;
		
		/*
		
		- Criação do teste:
		{"calendario":{"criacao":"2023-08-06T02:04:08.331Z","expiracao":3600},"status":"ATIVA","txid":"MAOER9YMTREVJ4023386661691287448321","revisao":0,"location":"pix.sicoob.com.br/qr/payload/v2/b229a5b8-e1ac-4cc6-8437-ace54f47c3b5","devedor":{"cpf":"01691128104","nome":"Davi Nunes de França"},"valor":{"original":"1.00"},"chave":"30063355000190","solicitacaoPagador":"Teste de Pix Instantaneo","infoAdicionais":[{"nome":"Campo 1","valor":"Informação Adicional1 do PSP-Recebedor"},{"nome":"Campo 2","valor":"Informação Adicional2 do PSP-Recebedor"}],"brcode":"00020101021226900014br.gov.bcb.pix2568pix.sicoob.com.br/qr/payload/v2/b229a5b8-e1ac-4cc6-8437-ace54f47c3b55204000053039865802BR5925CLAUDIO LEVI DE OLIVEIRA 6013Nao_informado62070503***6304B314"}
		
		- Consulta do teste antes de pagar
		{"brcode":"00020101021226900014br.gov.bcb.pix2568pix.sicoob.com.br/qr/payload/v2/b229a5b8-e1ac-4cc6-8437-ace54f47c3b55204000053039865802BR5925CLAUDIO LEVI DE OLIVEIRA 6013Nao_informado62070503***6304B314","revisao":0,"txid":"MAOER9YMTREVJ4023386661691287448321","calendario":{"criacao":"2023-08-06T02:04:08.325Z","expiracao":3600},"status":"ATIVA","devedor":{"cpf":"01691128104","nome":"Davi Nunes de França"},"chave":"30063355000190","solicitacaoPagador":"Teste de Pix Instantaneo","valor":{"original":"1.00","modalidadeAlteracao":0},"permiteAlteracao":false,"infoAdicionais":[{"nome":"Campo 1","valor":"Informação Adicional1 do PSP-Recebedor"},{"nome":"Campo 2","valor":"Informação Adicional2 do PSP-Recebedor"}],"payloadURL":"pix.sicoob.com.br/qr/payload/v2/b229a5b8-e1ac-4cc6-8437-ace54f47c3b5","pix":[]}
		
		- QRCODE: https://api.qrserver.com/v1/create-qr-code/?size=200x200&data=00020101021226900014br.gov.bcb.pix2568pix.sicoob.com.br/qr/payload/v2/b229a5b8-e1ac-4cc6-8437-ace54f47c3b55204000053039865802BR5925CLAUDIO%20LEVI%20DE%20OLIVEIRA%206013Nao_informado62070503***6304B314
		
		
		- Consulta do teste após pagamento
		
		{"brcode":"00020101021226900014br.gov.bcb.pix2568pix.sicoob.com.br/qr/payload/v2/b229a5b8-e1ac-4cc6-8437-ace54f47c3b55204000053039865802BR5925CLAUDIO LEVI DE OLIVEIRA 6013Nao_informado62070503***6304B314","revisao":0,"txid":"MAOER9YMTREVJ4023386661691287448321","calendario":{"criacao":"2023-08-06T02:04:08.325Z","expiracao":3600},"status":"CONCLUIDA","devedor":{"cpf":"01691128104","nome":"Davi Nunes de França"},"chave":"30063355000190","solicitacaoPagador":"Teste de Pix Instantaneo","valor":{"original":"1.00","modalidadeAlteracao":0},"permiteAlteracao":false,"infoAdicionais":[{"nome":"Campo 1","valor":"Informação Adicional1 do PSP-Recebedor"},{"nome":"Campo 2","valor":"Informação Adicional2 do PSP-Recebedor"}],"payloadURL":"pix.sicoob.com.br/qr/payload/v2/b229a5b8-e1ac-4cc6-8437-ace54f47c3b5","pix":[{"endToEndId":"E18236120202308060206s005cfe4b41","txid":"MAOER9YMTREVJ4023386661691287448321","valor":"1.00","horario":"2023-08-06T02:06:51.236Z","nomePagador":"Davi Nunes de Franca","pagador":{"nome":"Davi Nunes de Franca","cpf":"01691128104"},"devolucoes":[]}]}
		
		- Solicitação de Devolução:
		
		{"id":"1000","rtrId":"D02338666202308060229b3J0fPgbe2v","valor":"1.00","horario":{"solicitacao":"2023-08-06T02:29:53.183Z"},"status":"EM_PROCESSAMENTO"}
		
		- Consulta Estorno:
		
		{"id":"1000","rtrId":"D02338666202308060229b3J0fPgbe2v","valor":"1.00","horario":{"solicitacao":"2023-08-06T02:29:54.150Z","liquidacao":"2023-08-06T02:29:54.150Z"},"status":"DEVOLVIDO"}
		
		*/

		// Define os parâmetros no payload
		//O campo solicitacaoPagador, opcional, determina um texto a ser apresentado ao pagador para que ele possa digitar uma informação correlata, em formato livre, a ser enviada ao recebedor. Esse texto será preenchido, na pacs.008, pelo PSP do pagador, no campo RemittanceInformation . O tamanho do campo na pacs.008 está limitado a 140 caracteres.
		
		/*
		"infoAdicionais": [
						{
						  "nome": "titulo",
						  "valor": "'.$data['titulo'].'"
						},
						{
						  "nome": "valor",
						  "valor": "valor original"
						}
					  ]
		*/
		$jsonData = '
			{
			  "calendario": {
				"expiracao": 90
			  },
			  "devedor": {
				"'.$data['documento'].'": "'.$data['numDoc'].'",
				"nome": "'.$data['nome'].'"
			  },
			  "valor": {
				"original": "'.$data['valor'].'"
			  },
			  "chave": "30063355000190",
			  "solicitacaoPagador": "'.$data['obs'].'",
			  '.$data['infoAdicionais'].'
			}
		';


		$ch = curl_init($url); // Descomentar essa Linha!!

		// Define as opções da solicitação
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_VERBOSE, true); // Habilita depuração
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData); // Dados no formato JSON
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'Content-Type: application/json',
			'client_id: 95841f2c-228e-4171-a77d-dc94bf68de2f',
			'Accept: application/json',
			'Authorization: Bearer ' .$token,
		]);
		// var_dump ($token);
		// Define o certificado PFX
		curl_setopt($ch, CURLOPT_SSLCERT, "/var/www/ilunne/boss/sicoob/certificado.pem");
		curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $pfxPassword);
		$pfxPassword = $passwordCertificado;

		// Executa a solicitação e obtém a resposta
		$response = curl_exec($ch);

		// Verifica se ocorreu algum erro na solicitação
		if (curl_errno($ch)) {
			return 'Erro na requisição: ' . curl_error($ch);
		} else {
			return $response;
		}

		curl_close($ch);
		
	}
	
	function getPixImediato($token, $txid) { // Consulta o status de um Pix Imediato
		$url = "https://api.sicoob.com.br/pix/api/v2/cob/$txid";
		global $passwordCertificado;

		// Define os parâmetros no payload
		// $param = [
			// 'revisao' => '0',
		// ];
		// $url .= '?' . http_build_query($param);
		
		// echo $url;

		$ch = curl_init($url); // Inicializa uma nova sessão cURL

		// Define as opções da solicitação
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'Authorization: Bearer ' . $token,
			'client_id: 95841f2c-228e-4171-a77d-dc94bf68de2f',
			'Accept: application/json',
		]);

		// Define o certificado PFX
		curl_setopt($ch, CURLOPT_SSLCERT, "/var/www/ilunne/boss/sicoob/certificado.pem");
		curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $pfxPassword);
		$pfxPassword = $passwordCertificado;

		// Executa a solicitação e obtém a resposta
		$response = curl_exec($ch);

		// Verifica se ocorreu algum erro na solicitação
		if (curl_errno($ch)) {
			return 'Erro na requisição: ' . curl_error($ch);
		} else {
			return $response;
		}

		curl_close($ch);
	}
	
	function getPixe2eid($token, $e2eid) { // Consulta um  Pix pelo e2eid
		$url = "https://api.sicoob.com.br/pix/api/v2/pix/$e2eid";
		global $passwordCertificado;

		// Define os parâmetros no payload
		// $param = [
			// 'revisao' => '0',
		// ];
		// $url .= '?' . http_build_query($param);
		
		// echo $url;

		$ch = curl_init($url); // Inicializa uma nova sessão cURL

		// Define as opções da solicitação
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'Authorization: Bearer ' . $token,
			'client_id: 95841f2c-228e-4171-a77d-dc94bf68de2f',
			'Accept: application/json',
		]);

		// Define o certificado PFX
		curl_setopt($ch, CURLOPT_SSLCERT, "/var/www/ilunne/boss/sicoob/certificado.pem");
		curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $pfxPassword);
		$pfxPassword = $passwordCertificado;

		// Executa a solicitação e obtém a resposta
		$response = curl_exec($ch);

		// Verifica se ocorreu algum erro na solicitação
		if (curl_errno($ch)) {
			return 'Erro na requisição: ' . curl_error($ch);
		} else {
			return $response;
		}

		curl_close($ch);
	}	
	
	function estornaPix($token, $e2eid, $valor=1) { // Estorna um pix ==> [DESATIVADO] <==
		$url = "https://api.sicoob.com.br/pix/api/v2/pix/$e2eid/devolucao/1";
		global $passwordCertificado;

		// $ch = curl_init($url); // Descomentar essa linha

		// Define as opções da solicitação
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'Authorization: Bearer ' . $token,
			'client_id: 95841f2c-228e-4171-a77d-dc94bf68de2f',
			'Accept: application/json',
			'Content-Type: application/json',
		]);
		
		$jsonData = '{
			  "valor": '.$valor.'
			}';
			
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData); // Dados no formato JSON


		// Define o certificado PFX
		curl_setopt($ch, CURLOPT_SSLCERT, "/var/www/ilunne/boss/sicoob/certificado.pem");
		curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $pfxPassword);
		$pfxPassword = $passwordCertificado;

		// Executa a solicitação e obtém a resposta
		$response = curl_exec($ch);

		// Verifica se ocorreu algum erro na solicitação
		if (curl_errno($ch)) {
			return 'Erro na requisição: ' . curl_error($ch);
		} else {
			return $response;
		}

		curl_close($ch);
	}
		
	function consultaEstornoPix($token, $e2eid) { // Consulta um estorno de Pix
		$url = "https://api.sicoob.com.br/pix/api/v2/pix/$e2eid/devolucao/1000";
		global $passwordCertificado;

		$ch = curl_init($url); // Inicializa uma nova sessão cURL

		// Define as opções da solicitação
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'Authorization: Bearer ' . $token,
			'client_id: 95841f2c-228e-4171-a77d-dc94bf68de2f',
			'Accept: application/json',
		]);
		


		// Define o certificado PFX
		curl_setopt($ch, CURLOPT_SSLCERT, "/var/www/ilunne/boss/sicoob/certificado.pem");
		curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $pfxPassword);
		$pfxPassword = $passwordCertificado;

		// Executa a solicitação e obtém a resposta
		$response = curl_exec($ch);

		// Verifica se ocorreu algum erro na solicitação
		if (curl_errno($ch)) {
			return 'Erro na requisição: ' . curl_error($ch);
		} else {
			return $response;
		}

		curl_close($ch);
	}
			
	function getExtratoPix($token, $inicio, $fim) { // Consulta Recebimentos Pix
		$url = "https://api.sicoob.com.br/pix/api/v2/pix";
		global $passwordCertificado;
		
		//Define os parâmetros no payload
		// $dataSelecionada . "T00:00:00-03:00"
		$param = [
			'inicio' => $inicio . "T00:00:00-03:00",
			'fim' => $fim . "T23:59:59-03:00",
		];
		$url .= '?' . http_build_query($param);
		
		// echo $url;

		$ch = curl_init($url); // Inicializa uma nova sessão cURL

		// Define as opções da solicitação
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'Authorization: Bearer ' . $token,
			'client_id: 95841f2c-228e-4171-a77d-dc94bf68de2f',
			'Accept: application/json',
		]);
		


		// Define o certificado PFX
		curl_setopt($ch, CURLOPT_SSLCERT, "/var/www/ilunne/boss/sicoob/certificado.pem");
		curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $pfxPassword);
		$pfxPassword = $passwordCertificado;

		// Executa a solicitação e obtém a resposta
		$response = curl_exec($ch);

		// Verifica se ocorreu algum erro na solicitação
		if (curl_errno($ch)) {
			return 'Erro na requisição: ' . curl_error($ch);
		} else {
			return $response;
		}

		curl_close($ch);
	}
	

	/* Metodos para Conta Corrente */


	function getExtrato($token, $mes, $ano) { // Consulta extrato da conta
		$url = "https://api.sicoob.com.br/conta-corrente/v2/extrato/$mes/$ano";
		global $passwordCertificado;

		// Define os parâmetros no payload
		$data = [
			'numeroContaCorrente' => '16.463-1',
		];
		$url .= '?' . http_build_query($data);
		
		// echo $url;

		$ch = curl_init($url); // Inicializa uma nova sessão cURL

		// Define as opções da solicitação
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'Content-Type: application/json',
			'client_id: 95841f2c-228e-4171-a77d-dc94bf68de2f',
			'Accept: application/json',
			'Authorization: Bearer ' . $token
		]);

		// Define o certificado PFX
		curl_setopt($ch, CURLOPT_SSLCERT, "/var/www/ilunne/boss/sicoob/certificado.pem");
		curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $pfxPassword);
		$pfxPassword = $passwordCertificado;

		// Executa a solicitação e obtém a resposta
		$response = curl_exec($ch);

		// Verifica se ocorreu algum erro na solicitação
		if (curl_errno($ch)) {
			return 'Erro na requisição: ' . curl_error($ch);
		} else {
			return $response;
		}

		curl_close($ch);
	}
	
?>