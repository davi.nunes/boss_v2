<?php
	header('Content-Type: text/html; charset=UTF-8');
	
	include "api.php";
	require "../remessa/database.php";
	
	$token = $tkn = getToken();
		
	if($_GET['cpf']){
		$cpf = $_GET['cpf'];
	

			echo getBoletosCpf($tkn,$cpf);

		
	}
	
	if($_GET['nossoNumero']){
		$atributo = "nossoNumero";
		$valor= $_GET['nossoNumero'];

		$busca = getBoletoIndividual($tkn,$atributo,$valor);
		
			echo $busca;

		
	}	
	
	if($_GET['cpfCnpj']){
		$atributo = "cpfCnpj";
		$valor= $_GET['cpfCnpj'];

		$busca = getBoletosCpf($tkn, $valor);
		
			echo $busca;

		
	}	
	
	if($_GET['segundaVia']){

		$atributo = "nossoNumero";
		$valor= $_GET['segundaVia'];

		$busca = getSegundaViaPDF($tkn,$atributo,$valor);
		
			echo $busca;

		
	}	
	
	if($_GET['extrato']){

		$mes= $_GET['mes'];
		$ano= $_GET['ano'];

		$busca = getExtrato($tkn,$mes,$ano);
		
			echo $busca;

		
	}	
	
	if($_GET['criaBoleto']){

		// $mes= $_GET['mes'];
		// $ano= $_GET['ano'];

		$busca = postCriaBoleto($tkn);
		
			echo $busca;

		
	}
	
	if($_GET['setPix']){

		$atributo = "nossoNumero";
		$valor= $_GET['setPix'];

		$busca = setPix($tkn,$atributo,$valor);
		
			echo $busca;

		
	}
	
	if($_GET['criaPix']){
		
		$data['documento'] = $_GET['documento'];
		$data['numDoc'] = $_GET['numDoc'];
		$data['nome'] = $_GET['nome'];
		$data['valor'] = $_GET['valor'];
		$data['obs'] = $_GET['obs'];
		$data['titulo'] = $_GET['titulo'];
		$data['infoAdicionais'] = '';

		$busca = pixImediato($tkn, $data);
		
		echo $busca;

		
	}
	
	if($_GET['consultaPix']){
		
		$txid = $_GET['txid'];

		$busca = getPixImediato($tkn, $txid);
		
		echo $busca;

		
	}
	
	if($_GET['consultaPixe2eid']){
		
		$e2eid = $_GET['e2eid'];

		$busca = getPixe2eid($tkn, $e2eid);
		
		echo $busca;

		
	}
	
	if($_GET['estornaPix']){
		
		$e2eid = $_GET['e2eid'];
		$valor = $_GET['valor'];

		$busca = estornaPix($tkn, $e2eid, $valor);
		
		echo $busca;

		
	}	
	
	if($_GET['extratoPix']){
		
		$inicio = $_GET['inicio'];
		$fim = $_GET['fim'];

		$busca = getExtratoPix($tkn, $inicio, $fim);
		
		echo $busca;

		
	}	
	
	if($_GET['getEstornoPix']){
		
		$e2eid = $_GET['e2eid'];

		$busca = consultaEstornoPix($tkn, $e2eid);
		
		echo $busca;

		
	}	
	
	if($_GET['baixaBoleto']){

		$valor= $_GET['baixaBoleto'];

		$busca = patchBaixaBoleto($tkn,$valor);
		
			echo $busca;

		
	}
	
	if($_GET['movimento']){

		// $atributo = "nossoNumero";
		$dataSelecionada= $_GET['dataSelecionada'];
		$filtroStatus= $_GET['filtroStatus'];

		$busca = postSolicitaMovimento($tkn, $dataSelecionada, $filtroStatus);
		
			echo $busca;
	}	
	
	if($_GET['consultaMovimento']){

		// $atributo = "nossoNumero";
		$valor= $_GET['consultaMovimento'];

		$busca = getConsultaStatusMovimento($tkn, $valor);
		
			echo $busca;
	}	
	
	if($_GET['downloadMovimento']){

		$movimento = $_GET['downloadMovimento'];
		$idArquivo= $_GET['idArquivo'];

		$busca = getDownloadMovimento($tkn, $movimento, $idArquivo);
		
			echo $busca;
	}
	
	if($_GET['linhaDigitavel']){
		$atributo = "linhaDigitavel";
		$valor= $_GET['linhaDigitavel'];

		$busca = getBoletoIndividual($tkn,$atributo,$valor);
		

			echo $busca;

		
	}
	
	if($_GET['codigoBarras']){
		$atributo = "codigoBarras";
		$valor= $_GET['codigoBarras'];

		$busca = getBoletoIndividual($tkn,$atributo,$valor);
		

			echo $busca;

		
	}
		
	if($_GET['cliente']){
	$resultado = getBoletoIxc($_GET['cliente']);


		echo json_encode($resultado);

	}
	
	if($_GET['clientes']){
		$listaNumerosTituloJSON = $_POST['listaNumerosTitulo'];
		
		// Converte a string JSON para um array no PHP
		$listaNumerosTitulo = json_decode($listaNumerosTituloJSON);
		
		foreach($listaNumerosTitulo as $boleto){
			$bol[$boleto] = getPagadorIxc($boleto);
		}
		
		$resultado;

		echo json_encode($bol);

	}

/**************
	Daqui pra Baixo Funções relacionadas a consulta ao Banco de Dados
***************/

	function getBoletoIxc($boleto){

	$sql  = " select c.razao, c.cnpj_cpf, (SELECT DISTINCT n.nosso_numero FROM fn_areceber_cedente n WHERE n.id_cobranca = r.id) AS nosso_numero, r.id, r.valor, r.obs, r.data_vencimento, r.status, r.linha_digitavel, r.* " ;
	$sql .= " from fn_areceber r " ;
	$sql .= " left join cliente c " ;
	$sql .= " on r.id_cliente = c.id " ;
	$sql .= " where " ;
	$sql .= " ( " ;
	$sql .= " c.razao like '%$boleto%' " ;
	$sql .= " or c.cnpj_cpf like '%$boleto%' " ;
	$sql .= " or r.id = '".substr($boleto, 0, -1)."' " ;
	$sql .= " ) " ;
	$sql .= " and r.status = 'A' " ;
	$sql .= " order by c.razao asc, r.id_contrato desc, r.data_vencimento desc " ;

	
	// echo $sql;
	$result	= DBExecute($sql);
	// var_dump($sql);
	if(!mysqli_num_rows($result)){

	}else{
		while($retorno = mysqli_fetch_assoc($result)){
			foreach ($retorno as $coluna => $valor) {
				// Alterar a codificação de cada valor
				$valorConvertido = iconv('ISO-8859-1', 'UTF-8', $valor);

				// Atualizar o valor da coluna no array de dados
				$retorno[$coluna] = $valorConvertido;
			}

			// var_dump($retorno);
			$dados[] = $retorno;
		}
	}
	
	return $dados;
	}
	
	function getPagadorIxc($boleto){

	$sql  = " select c.razao, c.cnpj_cpf, r.id, r.status " ;
	$sql .= " from fn_areceber r " ;
	$sql .= " left join cliente c " ;
	$sql .= " on r.id_cliente = c.id " ;
	$sql .= " where r.id = $boleto" ;

	
	// echo $sql;
	$result	= DBExecute($sql);
	// var_dump($sql);
	if(!mysqli_num_rows($result)){

	}else{
		while($retorno = mysqli_fetch_assoc($result)){
			foreach ($retorno as $coluna => $valor) {
				// Alterar a codificação de cada valor
				$valorConvertido = iconv('ISO-8859-1', 'UTF-8', $valor);

				// Atualizar o valor da coluna no array de dados
				$retorno[$coluna] = $valorConvertido;
			}

			// var_dump($retorno);
			$dados[] = $retorno;
		}
	}
	
	return $dados;
	}


?>