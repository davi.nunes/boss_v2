<?php
header("Content-Type: text/html; charset=UTF-8");
if(!isset($_GET['txid'])) exit; // Se não tem titulo não há o que fazer

include "/var/www/ilunne/boss/sicoob/api.php";

$txid = $_GET['txid'];

$busca = getPixImediato(getToken(), $txid);

$obj = json_decode($busca);

$status = $obj->status;
$pix= $obj->pix;

//Verifica se esse pix é relacionado a um boleto no ixc
if($obj->infoAdicionais[0]->nome == "titulo"){
	$titulo = $obj->infoAdicionais[0]->valor;
	include "fun.php";
	$boletoIxc = getBoletoIxc($titulo)[0];
	$statusIxc = $boletoIxc['status'];
}else{
	echo "Não corresponde a um boleto";
	exit;
}

$busca  = formataJson($busca);

// "pix":[{"endToEndId":"E18236120202308060206s005cfe4b41","txid":"MAOER9YMTREVJ4023386661691287448321","valor":"1.00","horario":"2023-08-06T02:06:51.236Z","nomePagador":"Davi Nunes de Franca","pagador":{"nome":"Davi Nunes de Franca","cpf":"01691128104"},"devolucoes":[]}]

if($obj->status == "ATIVA" and !isExpired($obj)){ // Se está ativa e não expirou
	echo $busca;
}elseif($obj->status == "CONCLUIDA"){
	if($statusIxc != "R" and $statusIxc != "C"){
		include "/var/www/ilunne/boss/api-ixc/api.php";
				
		$host = 'https://acessodf.net/webservice/v1';
		$token = '6:e6a38134ca5be7e571282d7cca2e0eb9420b32b152aa701e6ad5127306535277';//token gerado no cadastro do usuario (verificar permissões)
		$selfSigned = false; //true para certificado auto assinado
		$api = new IXCsoft\WebserviceClient($host, $token, $selfSigned);
		$params = array(
					'filial_id' => '2', // filial do recebimento
					'id_receber' => $titulo, // id do título
					'documento' => $pix[0]->endToEndId,
					'conta_' => '33', // conta que está fazendo o recebimento
					'id_conta' => '1', // planejamento analítico da conta que está fazendo o recebimento
					'tipo_recebimento' => 'P', // tipo recebimento em PIX
					'data' => date("d/m/Y", strtotime($pix[0]->horario)), // data do recebimento
					'valor_parcela' => $pix[0]->valor,   // valor da parcela
					'credito' => $pix[0]->valor, // credito e débito devem se anular na baixa
					'debito' => $pix[0]->valor,
					'valor_total_recebido' => $pix[0]->valor, // valor total
					'historico' => "Recebido via PIX de {$pix['0']->nomePagador}. TXID: {$pix['0']->txid}", // descrição registrada
					'previsao' => 'N', // Previsão = S significa isentar de um período, previsão = significa que foi cobrado
					'tipo_lanc' => 'R', // tipo de lançamento recebimento
				);
		$api->post('fn_areceber_recebimentos_baixas_novo', $params);//Faz a o POST na API na tabela
		$retorno = $api->getRespostaConteudo(false);// false para json | true para array

	}
	if(!isset($retorno)) $retorno = "";
	$objetoResultado = json_decode($busca, true);
	$objetoResultado['ixc_response']['titulo'] = $statusIxc;
	$objetoResultado['ixc_response']['baixa'] = json_decode($retorno, true);
	
	echo formataJson(json_encode($objetoResultado));
	// echo $statusIxc;
}else{ // Se está ativa mas expirou
	$remover = delQrDatabase($titulo, $obj->txid);
	// var_dump($remover);
	echo $busca;
	// echo "Se está ativa mas expirou";
	
}

// var_dump(isExpired($criacao,$expiracao));
// var_dump($pix);
// var_dump($boletoIxc);
// var_dump($obj->infoAdicionais[0]->valor);


function isExpired($obj) {
	
	$creationDate = $obj->calendario->criacao;
	$expirationSeconds = $obj->calendario->expiracao;
	
    // Converter a data de criação para um objeto DateTime
    $creationDateTime = new DateTime($creationDate);
    
    // Obter a data e hora atual
    $currentDateTime = new DateTime();
    
    // Calcular o intervalo de tempo entre a data de criação e a data atual em segundos
    $timeDifference = $currentDateTime->getTimestamp() - $creationDateTime->getTimestamp();
    
    // Verificar se o intervalo de tempo é maior ou igual ao tempo de expiração em segundos
    if ($timeDifference >= $expirationSeconds) {
        return true; // Expirado
    } else {
        return false; // Não expirado
    }
}

?>