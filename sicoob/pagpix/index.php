
  <head>
    <title>Pague com Pix</title>
    <link href="meu.css?<?php echo time();?>" rel="stylesheet">
	<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
	<script src="meu.js?<?php echo time();?>"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>
<div class="container">

<?php

header("Content-Type: text/html; charset=UTF-8");

if(!isset($_GET['titulo'])) exit; // Se não tem titulo não há o que fazer

$boleto = $_GET['titulo'];

include "fun.php";

$boleto = getBoletoIxc($boleto)['0'];

// Se não encontrou o boleto:
if(!$boleto['id']){
	echo "Titulo não localizado!";
	exit;
}
// Se não encontrou o boleto:
if($boleto['status'] == 'R'){
	$iframeURL = 'https://acessodf.net/pagpix/pdf/?boleto='.$boleto['identificador'];
	// var_dump($boleto);
	echo "<embed  src='$iframeURL' type='application/pdf' width='100%' height='100%'/>";
	exit;
	
}// Se não encontrou o boleto:
if($boleto['status'] != 'A'){
	echo "Titulo não está em aberto!";
	exit;
}

// Se eu tenho anotações sobre o boleto:
$notas = getQrDatabase($boleto['identificador']);

// Se está expirada e ativa, faz bem eu verificar por desencargo de consciencia:
if($notas and $notas[0]['e2eid'] == 'NULL' and $notas[0]['status'] == '0'){
	echo "Tratar a questão do expirado";
	$restam = 1;
	$expiracao = "calendario='venceu' ciclo='1' restam='$restam'";
}

// Faço uma chamada ao txid e dependendo do retorno eu continuo


// Fim codigo de verificação

echo "<div class='titulo'>Resumo da fatura</div>";
echo "<table class='resumo'>";
echo "<tr>";
echo "<th colspan='20'>";
echo $boleto['razao'];
echo " <br/> ";
echo $boleto['cnpj_cpf'];
echo "  ";
echo $boleto['id_contrato'];
echo "</th>";
echo "</tr>";

echo "<tr>";
	echo "<td>";
		echo "Identificador";
	echo "</td>";
	echo "<td>";
		echo $boleto['identificador'];
	echo "</td>";
echo "</tr>";
echo "<tr>";
	echo "<td>";
		echo "Valor";
	echo "</td>";
	echo "<td>";
		echo formatarMoeda($boleto['valor']);
	echo "</td>";
echo "</tr>";
echo "<tr>";
	echo "<td>";
		echo "Multa";
	echo "</td>";
	echo "<td>";
		echo (float) $boleto['multap']."%";
	echo "</td>";
echo "</tr>";
echo "<tr>";
	echo "<td>";
		echo "Juros";
	echo "</td>";
	echo "<td>";
		echo (float) $boleto['jurop']."% ao dia";
	echo "</td>";
echo "</tr>";
echo "<tr id='valor-hoje'>";
	echo "<td>";
		echo "Valor Hoje";
	echo "</td>";
	echo "<td>";
		echo formatarMoeda(calcularJurosBoleto($boleto['valor'], $boleto['data_vencimento'], $boleto['jurop'], $boleto['multap']));
	echo "</td>";
echo "</tr>";
echo "<tr>";
	echo "<td>";
		echo "Descrição";
	echo "</td>";
	echo "<td>";
		echo $boleto['obs'];
	echo "</td>";
echo "</tr>";
echo "<tr>";
	echo "<td>";
		echo "Vencimento";
	echo "</td>";
	echo "<td>";
		echo date("d/m/Y", strtotime($boleto['data_vencimento']));
	echo "</td>";

echo "</tr>";
echo "</table>";

// Se eu tiver notas, populo com os dados da nota que tenho e escondo o botão

$botVisivel = $notas ? "style='display:none;'" : " ";
$txidVisivel = $notas ? " " : "style='display:none;'";
$loading = $notas ? " " : "style='display:none;'";

if($notas and $notas[0]['e2eid'] != 'NULL'){
	$qrCodetxt = "Consta Transação relacionada a este QRCODE em andamento. Solicite ao time de suporte para verificar ou aguarde...";
}else{
	$qrCodetxt = $notas[0]['qrcode'];
}

if($notas[0]['status'] == '1'){
	$restam = calcularTempoRestanteEmSegundos($notas[0]['calendario'], $notas[0]['ciclo']);
	$expiracao = "calendario='{$notas[0]['calendario']}' ciclo='{$notas[0]['ciclo']}' restam='{$restam}'";
	// echo $restam;
	
}

$qrCode = $notas ? "<img src='https://api.qrserver.com/v1/create-qr-code/?size=200x200&data=".$notas['0']['qrcode']."'>" : "<div class='loading-effect'></div>";
$txid = $notas ? $notas[0]['txid'] : "";

echo "<button $botVisivel id='pagpix' class='toggle' titulo='{$boleto['identificador']}'>Pagar com Pix!</button>";
echo "<div $txidVisivel id='txid' class='toggle' >$txid</div>";
echo "<div class='expiracao-container'><div id='expiracao' $expiracao class='toggle' $loading></div></div>";

echo "<div id='pagpixqr'>$qrCode</div>";
echo "<div id='qrtxt'>$qrCodetxt</div>";
// var_dump($notas);

?>

</div>