<?php
header("Content-Type: text/html; charset=UTF-8");
if(!isset($_GET['titulo'])) exit; // Se não tem titulo não há o que fazer

$boleto = $_GET['titulo'];

include "fun.php";
include "/var/www/ilunne/boss/sicoob/api.php";

$boleto = getBoletoIxc($boleto)['0'];

$tipoCob = $boleto['id_contrato'] == "0" ? "Avulso" : "Ref. contrato numero ".$boleto['id_contrato'];
$boleto['identificador'];
$boleto['valor'];
$boleto['multap'];
$boleto['jurop'];
$boleto['data_vencimento'];
$valorHoje = number_format(calcularJurosBoleto($boleto['valor'], $boleto['data_vencimento'], $boleto['jurop'], $boleto['multap']), 2, ',', '.');
$valorHoje = str_replace(',', '.', $valorHoje);

$infoAdicionais = '"infoAdicionais": [
				{
				  "nome": "titulo",
				  "valor": "'.$boleto['identificador'].'"
				},
				{
				  "nome": "valor original",
				  "valor": "'.$boleto['valor'].'"
				},
				{
				  "nome": "valor hoje",
				  "valor": "'.$valorHoje.'"
				},
				{
				  "nome": "tipo de titulo",
				  "valor": "'.$tipoCob.'"
				}
			  ]';
	
$data['documento'] = detectarTipo($boleto['cnpj_cpf']);
$data['numDoc'] = limparCpfCnpj($boleto['cnpj_cpf']);
$data['nome'] = $boleto['razao'];
$data['valor'] = $valorHoje;
$data['obs'] = $boleto['obs'];
$data['titulo'] = $boleto['identificador'];
$data['infoAdicionais'] = $infoAdicionais;


$pix = pixImediato(getToken(),$data);
$db = json_decode($pix);

$nota['id_titulo'] = $boleto['identificador'];
$nota['txid'] = $db->txid;
$nota['calendario'] = $db->calendario->criacao;
$nota['ciclo'] = $db->calendario->expiracao;
$nota['qrcode'] = $db->brcode;
$nota['e2eid'] = 'NULL';

$databases = updateDatabase($nota);

echo $pix;

?>