$(document).ready(function() {
	
	if($("#expiracao").attr("calendario") !== undefined){
		console.log("Não preciso esperar clicar");
		const tempoEsvaziamento = $("#expiracao").attr("restam") * 1000; // Converta para milissegundos
		var dynamicCSS = `
		  .expiracao-loading::before {
			animation-duration: ${tempoEsvaziamento}ms;
		  }
		`;

		const styleElement = document.createElement("style");
		styleElement.type = "text/css";
		styleElement.appendChild(document.createTextNode(dynamicCSS));

		document.head.appendChild(styleElement);

		$("#expiracao").html(`<span>Expira em: ${obterDataExpiracaoDb($("#expiracao").attr("calendario"), $("#expiracao").attr("ciclo"))}</span>`);
		$("#expiracao").addClass("expiracao-loading");

		setTimeout(function() {
		  $("#expiracao").removeClass("expiracao-loading");
		  styleElement.remove(); // Remova o estilo dinâmico
		}, tempoEsvaziamento);
		
		const verificacaoInterval = setInterval(function() {
			verificarTransacaoRecebida($("#txid").text(), verificacaoInterval);
		}, 2000);
		
	}
	
	$("#pagpix").click(function() {
		let titulo = $(this).attr("titulo");
		const url = `action.php?titulo=${titulo}`;
		$('.toggle').toggle();

		$.ajax({
		  url: url,
		  dataType: "json",
		  success: function(data) {
			console.log(data.txid);
			$("#qrtxt").html(data.brcode);
			$("#txid").html(data.txid);
			$("#pagpixqr").html(criarQRCode(data.brcode));

			const tempoEsvaziamento = data.calendario.expiracao * 1000; // Converta para milissegundos
			var dynamicCSS = `
			  .expiracao-loading::before {
				animation-duration: ${tempoEsvaziamento}ms;
			  }
			`;

			const styleElement = document.createElement("style");
			styleElement.type = "text/css";
			styleElement.appendChild(document.createTextNode(dynamicCSS));

			document.head.appendChild(styleElement);

			$("#expiracao").html(`<span>Expira em: ${obterDataExpiracao(data.calendario.criacao, data.calendario.expiracao)}</span>`);
			$("#expiracao").addClass("expiracao-loading");

			setTimeout(function() {
			  $("#expiracao").removeClass("expiracao-loading");
			  styleElement.remove(); // Remova o estilo dinâmico
			}, tempoEsvaziamento);
			
			const verificacaoInterval = setInterval(function() {
				verificarTransacaoRecebida(data.txid, verificacaoInterval);
			}, 2000);
		  },
		  error: function(jqXHR, textStatus, errorThrown) {
			console.log("Erro na solicitação AJAX: " + textStatus);
			console.log("Detalhes do erro: " + errorThrown);
			$('.toggle').toggle();
		  }
		});
  });

	$("#qrtxt").click(function() {
		var texto = $(this).text();
		var $tempInput = $("<input>");
		$("body").append($tempInput);
		$tempInput.val(texto).select();
		document.execCommand("copy");
		$tempInput.remove();

		// Substitui o texto por "Copiado!" por 3 segundos
		$(this).text("Copiado!");
		setTimeout(function() {
		  $("#qrtxt").text(texto);
		}, 1500);
	  });
});


function criarQRCode(qrData) {

  // Cria o elemento img com o src baseado no canvas
  var img = document.createElement("img");
  img.src = "https://api.qrserver.com/v1/create-qr-code/?size=200x200&data="+qrData;

  return img;
}

function obterDataExpiracao(criacaoISO, expiracao) {
  const criacaoDate = new Date(criacaoISO);
  const expiracaoDate = new Date(criacaoDate.getTime() + expiracao * 1000);
  // expiracaoDate.setUTCHours(expiracaoDate.getUTCHours() - 3);
  const expiracaoLocal = expiracaoDate.toLocaleString();
  return expiracaoLocal;

}

function obterDataExpiracaoDb(calendarioMySQL, expiracao) {
  const criacaoDate = new Date(calendarioMySQL.replace(/-/g, "/")); // Converter para o formato de data padrão
  const expiracaoDate = new Date(criacaoDate.getTime() + expiracao * 1000);
  // expiracaoDate.setUTCHours(expiracaoDate.getUTCHours() - 3);
  const expiracaoLocal = expiracaoDate.toLocaleString();
  return expiracaoLocal;
}

function verificarTransacaoRecebida(txid, interval) {
  $.ajax({
    url: `consulta.php?txid=${txid}`,
    dataType: "json",
    success: function(data) {
		console.log(data.status);
      if (data.status == 'ATIVA' && !expirou(data.calendario.criacao, data.calendario.expiracao)) {
        // Faça o que você deseja após a transação ser recebida
        console.log("Em aberto!");
      }else if(data.status == 'CONCLUIDA'){
			console.log("Foi Pago!");
			$("#expiracao").html("Titulo PAGO!");
			$("#qrtxt").html(data.pix[0].endToEndId);
			let iframeURL = 'https://acessodf.net/pagpix/pdf/?boleto='+data.infoAdicionais[0].valor;
			$("#pagpixqr").html("<embed  src='"+iframeURL+"' type='application/pdf' width='100%' height='100%'/>");
			
			clearInterval(interval); // Para a verificação após a transação ser recebida
	  }else if(expirou(data.calendario.criacao, data.calendario.expiracao)){
		console.log("Expirou!");
		$("#expiracao").html("<span>QR CODE expirado! Recarregando a página em 3s!</span>");
		clearInterval(interval); // Para a verificação após a transação ser recebida
		setTimeout(function() {
			location.reload(); // Recarrega a página atual após 2 segundos
		}, 3500); // 3500 milissegundos = 2 segundos
	  } 
    },
    error: function(jqXHR, textStatus, errorThrown) {
      console.log("Erro na solicitação AJAX: " + textStatus);
      console.log("Detalhes do erro: " + errorThrown);
    }
  });
}

function expirou(criacaoISO, expiracao) {
  const criacaoDate = new Date(criacaoISO);
  const expiracaoDate = new Date(criacaoDate.getTime() + expiracao * 1000);
  const agora = new Date();

  return agora > expiracaoDate;
}