<?php //PagPix Sicoob

	require "/var/www/ilunne/boss/remessa/database.php";
	
	function getBoletoIxc($boleto){

	$sql  = " select c.razao, c.cnpj_cpf, r.id as identificador, r.valor, r.obs, r.data_vencimento, r.status, r.id_contrato, fcc.id, fcc.multap, fcc.jurop " ;
	$sql .= " from fn_areceber r " ;
	$sql .= " left join cliente c on r.id_cliente = c.id " ;
	$sql .= " left join fn_carteira_cobranca fcc on fcc.id = r.id_carteira_cobranca " ;
	$sql .= " where r.id = $boleto " ;
	$sql .= " order by c.razao asc, r.id_contrato desc, r.data_vencimento desc " ;

	
	// echo $sql;
	$result	= DBExecute($sql);
	// var_dump($sql);
	if(!mysqli_num_rows($result)){

	}else{
		while($retorno = mysqli_fetch_assoc($result)){
			foreach ($retorno as $coluna => $valor) {
				// Alterar a codificação de cada valor
				$valorConvertido = iconv('ISO-8859-1', 'UTF-8', $valor);
				// $valorConvertido = $valor;

				// Atualizar o valor da coluna no array de dados
				$retorno[$coluna] = $valorConvertido;
			}

			// var_dump($retorno);
			$dados[] = $retorno;
		}
	}
	
	return $dados;
	}	
	
	function getQrDatabase($boleto){

	$sql  = " SELECT id, id_titulo, txid, ";
	$sql .= " DATE_SUB(calendario, INTERVAL 3 HOUR) AS calendario, ";
	$sql .= " ciclo, status, e2eid, qrcode ";
	$sql .= " FROM pagpix.sicoob ";
	$sql .= " WHERE id_titulo = $boleto ";
	$sql .= " AND (status = 1 OR (status = 0 AND e2eid IS NOT NULL)) ";
	
	$dados = false;
	
	// echo $sql;
	$result	= DBExecute($sql);
	// var_dump($sql);
	if(!mysqli_num_rows($result)){

	}else{
		while($retorno = mysqli_fetch_assoc($result)){
			foreach ($retorno as $coluna => $valor) {
				// Alterar a codificação de cada valor
				$valorConvertido = iconv('ISO-8859-1', 'UTF-8', $valor);
				// $valorConvertido = $valor;

				// Atualizar o valor da coluna no array de dados
				$retorno[$coluna] = $valorConvertido;
			}

			// var_dump($retorno);
			$dados[] = $retorno;
		}
	}
	
	return $dados;
	}	
	
	function getDbRecibo($boleto){

	$sql  = "SELECT 
				c.razao as nome, 
				c.cnpj_cpf as cpf, 
				mov.id as id_mov, 
				mov.tipo_recebimento, 
				mov.historico, 
				boleto.numero_parcela_recorrente as parcela, 
				boleto.pagamento_valor, 
				DATE_FORMAT(boleto.pagamento_data, '%d/%m/%Y') as data, 
				boleto.status, 
				boleto.id as boleto, 
				boleto.obs, 
				mov.conta_, 
				mov.documento, 
				mov.credito ";
	$sql .= "FROM 
				fn_movim_finan AS mov ";
	$sql .= "LEFT JOIN 
				fn_areceber AS boleto ON mov.id_receber = boleto.id ";
	$sql .= "LEFT JOIN 
				cliente AS c ON c.id = boleto.id_cliente ";
	$sql .= "WHERE 
				boleto.id = $boleto";
// var_dump($sql);
	
	$dados = false;
	
	// echo $sql;
	$result	= DBExecute($sql);
	// var_dump($sql);
	if(!mysqli_num_rows($result)){

	}else{
		while($retorno = mysqli_fetch_assoc($result)){
			foreach ($retorno as $coluna => $valor) {
				// Alterar a codificação de cada valor
				$valorConvertido = iconv('ISO-8859-1', 'UTF-8', $valor);
				// $valorConvertido = $valor;

				// Atualizar o valor da coluna no array de dados
				$retorno[$coluna] = $valorConvertido;
			}

			// var_dump($retorno);
			$dados[] = $retorno;
		}
	}
	
	return $dados;
	}	
	
	function delQrDatabase($boleto, $txid){

	$sql  = " DELETE FROM pagpix.sicoob WHERE id_titulo = $boleto and txid = '$txid'; " ;
	
	$dados = false;
	
	// echo $sql;
	$result	= DBExecute($sql);
	// var_dump($sql);
	if(!mysqli_num_rows($result)){
		$dados = true;
	}else{
		while($retorno = mysqli_fetch_assoc($result)){
			foreach ($retorno as $coluna => $valor) {
				// Alterar a codificação de cada valor
				$valorConvertido = iconv('ISO-8859-1', 'UTF-8', $valor);
				// $valorConvertido = $valor;

				// Atualizar o valor da coluna no array de dados
				$retorno[$coluna] = $valorConvertido;
			}

			// var_dump($retorno);
			$dados[] = $retorno;
		}
	}
	
	return $dados;
	}	
	
	function updateDatabase($boleto){
		
		extract($boleto);

		$sql  = " INSERT INTO pagpix.sicoob (id_titulo, txid, calendario, ciclo, qrcode, e2eid) " ;
		$sql .= " VALUES ('{$id_titulo}', '{$txid}', STR_TO_DATE('{$calendario}', '%Y-%m-%dT%H:%i:%s.%fZ'), '{$ciclo}', '{$qrcode}', '{$e2eid}') " ;
		$sql .= " ON DUPLICATE KEY UPDATE " ;
		$sql .= " id_titulo = '{$id_titulo}',  " ;
		$sql .= " txid = '{$txid}',  " ;
		$sql .= " calendario = '{$calendario}', " ;
		$sql .= " ciclo = '{$ciclo}', " ;
		$sql .= " qrcode = '{$qrcode}',  " ;
		$sql .= " e2eid = '{$e2eid}' " ;

		
		// echo $sql;
		$result	= DBExecute($sql);
		// var_dump($sql);
		if(!mysqli_num_rows($result)){

		}else{
			while($retorno = mysqli_fetch_assoc($result)){
				foreach ($retorno as $coluna => $valor) {
					// Alterar a codificação de cada valor
					$valorConvertido = iconv('ISO-8859-1', 'UTF-8', $valor);
					// $valorConvertido = $valor;

					// Atualizar o valor da coluna no array de dados
					$retorno[$coluna] = $valorConvertido;
				}

				// var_dump($retorno);
				$dados[] = $retorno;
			}
		}
		
		return $dados;
	}
	
	function calcularJurosBoleto($valor, $vencimento, $juros, $multa) {
		// Verifica se o vencimento é sábado (6) ou domingo (0)
		$diaSemana = date('w', strtotime($vencimento));
		if ($diaSemana == 6) { // Sábado
			$vencimento = date('Y-m-d', strtotime($vencimento . ' +2 days'));
		} elseif ($diaSemana == 0) { // Domingo
			$vencimento = date('Y-m-d', strtotime($vencimento . ' +1 day'));
		}
		
		$agora = time();
		$dataVencimento = strtotime($vencimento);
		if ($agora > $dataVencimento) {
			
			// Calcula a quantidade de dias de atraso
			$hoje = strtotime(date('Y-m-d'));
			$dataVencimento = strtotime($vencimento);
			$diasAtraso = max(0, ($hoje - $dataVencimento) / (60 * 60 * 24));
			
			// Calcula o valor do juros e multa
			$jurosCalculado = $valor * ($juros / 100) * $diasAtraso;
			$multaCalculada = $valor * ($multa / 100);
			
			// Calcula o valor total a ser pago
			$valorTotal = $valor + $jurosCalculado + $multaCalculada;
			return $valorTotal;
		}else{
			return $valor;
		}
		
}

	function formatarMoeda($valor) {
		return 'R$ ' . number_format($valor, 2, ',', '.');
	}
	
	function limparCpfCnpj($cpfCnpj) {
		// Remove todos os caracteres não numéricos (deixa apenas os números)
		return preg_replace('/[^0-9]/', '', $cpfCnpj);
	}
	
	function detectarTipo($numero) {
		// Remove todos os caracteres não numéricos (deixa apenas os números)
		$numeroLimpo = preg_replace('/[^0-9]/', '', $numero);
		
		// Verifica o comprimento do número para determinar se é CPF ou CNPJ
		if (strlen($numeroLimpo) === 11) {
			return 'cpf';
		} elseif (strlen($numeroLimpo) === 14) {
			return 'cnpj';
		} else {
			return 'Tipo não identificado';
		}
	}

	function formataJson($jsonString) {
		$jsonObj = json_decode($jsonString);
		if ($jsonObj === null) {
			return "JSON inválido.";
		}
		return json_encode($jsonObj, JSON_PRETTY_PRINT);
	}

	function calcularTempoRestanteEmSegundos($calendario, $ciclo) {
		$dataCalendario = new DateTime($calendario);
		$dataExpiracao = $dataCalendario->add(new DateInterval('PT' . $ciclo . 'S'));

		$agora = new DateTime();
		$intervalo = $agora->diff($dataExpiracao);

		$tempoRestanteSegundos = $intervalo->s + ($intervalo->i * 60) + ($intervalo->h * 3600) + ($intervalo->d * 86400);

		return $tempoRestanteSegundos;
	}
/* 

Crie um script para gerar, no mariadb, o seguinte banco:

Banco: pagpix

Tabela: sicoob

Campos:

id: autoincrement, primary key
id_titulo: receberá um valor numerico não negativo de até 30 digitos
txid: exemplo de dado: MAOER9YMTREVJ4023386661691287448321
calendario: exemplo de dado: 2023-08-06T02:04:08.331Z
ciclo: receberá um valor numerico não negativo de até 30 digitos
status: campo calculado (deve calcular, com base no valor do campo calendário e ciclo, se esta trupa está expirada, respondendo true para não expirada e false para expirada)

CREATE DATABASE IF NOT EXISTS pagpix;

USE pagpix;

CREATE TABLE IF NOT EXISTS sicoob (
    id INT AUTO_INCREMENT PRIMARY KEY,
    id_titulo BIGINT UNSIGNED NOT NULL,
    txid VARCHAR(50) NOT NULL,
    calendario DATETIME NOT NULL,
    ciclo BIGINT UNSIGNED NOT NULL,
    status BOOLEAN AS (TIMESTAMPDIFF(SECOND, calendario, NOW()) < ciclo)
);

ALTER TABLE sicoob ADD INDEX idx_calendario (calendario);


-- Exemplo de inserção de dados
INSERT INTO sicoob (id_titulo, txid, calendario, ciclo)
     VALUES ('376724', 'TAURPCWHWAMLCZ023386661691429992006', STR_TO_DATE('2023-08-07T17:39:52.008Z', '%Y-%m-%dT%H:%i:%s.%fZ'), '120');

-- Consulta para verificar os resultados
SELECT * FROM sicoob WHERE id_titulo = 376724 AND (status = 1 OR (status = 0 AND e2eid IS NOT NULL));

INSERT INTO sicoob (id_titulo, txid, calendario, ciclo)
VALUES ('376724', 'TAURPCWHWAMLCZ023386661691429992006', STR_TO_DATE('2023-08-07T17:39:52.008Z', '%Y-%m-%dT%H:%i:%s.%fZ'), '120');
ON DUPLICATE KEY UPDATE id_titulo = valor1, txid = valor2, calendario = valor3, ciclo = valor4;


*/
?>