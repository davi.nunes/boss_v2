<?php

header("Content-Type: text/html; charset=UTF-8");
require('fpdf.php'); // Inclua o arquivo da biblioteca FPDF

include "/var/www/ilunne/boss/sicoob/pagpix/fun.php";

$boleto = $_GET['boleto'];
$dados_consulta = getDbRecibo($boleto)[0];


/*
array(1) { [0]=> array(10) { ["id"]=> string(7) "1022589" ["tipo_recebimento"]=> string(1) "B" ["historico"]=> string(27) "Rec. DAVI NUNES DE FRANÇA" ["numero_parcela_recorrente"]=> string(0) "" ["pagamento_valor"]=> string(5) "15.00" ["status"]=> string(1) "R" ["obs"]=> string(0) "" ["conta_"]=> string(2) "32" ["documento"]=> string(6) "376028" ["credito"]=> string(5) "15.00" } }
*/

// Crie uma nova instância do FPDF com tamanho "Ticket"
$pdf = new FPDF();
$pdf->SetMargins(2, 2, 2);
$pdf->AddPage('P', array(80, 160)); // Largura x Altura



// Adicione a imagem da logomarca (substitua pelo caminho real da imagem)
$logo_path = "https://logos.speedtestcustom.com/prod/114132-1639962549953.png";
$image_width = 60; // Substitua pelo tamanho real da imagem
$image_height = 10; // Substitua pelo tamanho real da imagem

// Calcule a posição x para centralizar horizontalmente
$page_width = $pdf->GetPageWidth();
$image_x = ($page_width - $image_width) / 2;

$pdf->Image($logo_path, $image_x, 10, $image_width, $image_height, 'PNG');

$y_position = 25; // Posição incial do texto



// Defina a fonte e o tamanho do texto
$pdf->SetFont('Arial', '', 5.8);

printLine("ACESSO.COM SERVIÇOS DE TELECOMUNICAÇÕES E SUPRIMENTOS EIRELI");
$pdf->SetFont('Arial', '', 8);
printLine("CNPJ: 30.063.355/0001-90");
printLine("FONE: (61) 3479-1091");
printLine("Quadra 03 Bloco A loja 08 Sobreloja 01 Brazlandia/DF CEP: 72705-531");
// Adicione os dados da consulta

// $pdf->Line(2, $y_position+1, 78, $y_position+1);

$pdf->SetXY(2, novaLinha() );
$pdf->SetFont('Arial', '', 8);
$pdf->Cell(0, 5, "Comprovante de Pagamento", 'LTRB', 2, 'C', false );

novaLinha();
novaLinha();
printLine("Titular: {$dados_consulta['nome']}");
printLine("CPF/CNPJ: {$dados_consulta['cpf']}");
printLine("Cód. fatura: {$dados_consulta['boleto']}");
printLine("Cód. recebimento: {$dados_consulta['id_mov']}");
printLine("Tipo de recebimento: {$dados_consulta['tipo_recebimento']}");
printLine("Transação: {$dados_consulta['documento']}");
printLine("Conta: {$dados_consulta['conta_']}");
printLine("Parecela: {$dados_consulta['parcela']}");
printLine("Descrição: {$dados_consulta['obs']}");
printLine("Lançamento: {$dados_consulta['historico']}");
printLine("Data: {$dados_consulta['data']}");
printLine("Valor: {$dados_consulta['credito']}");
printLine("Este CUPOM tem validade de 1 ano à partir da data de emissão e não possui valor fiscal. A sua fatura completa e detalhada dos serviços prestados está disponível no endereço eletrônico: ");
printLine("acessodf.net");
// printLine("Tipo de recebimento: {$dados_consulta['pagamento_valor']}");

$logo_path = "https://api.qrserver.com/v1/create-qr-code/?size=200x200&data=acessodf.net/pagpix/pdf?boleto={$boleto}";
$image_width = 25; // Substitua pelo tamanho real da imagem
$image_height = 25; // Substitua pelo tamanho real da imagem
$page_width = $pdf->GetPageWidth();
$image_x = ($page_width - $image_width) / 2;
$pdf->Image($logo_path, $image_x, novaLinha(), $image_width, $image_height, 'PNG');


// Saída do PDF
$pdf->Output();

function novaLinha(){
	global $y_position;
	$y_position += 4;
	return $y_position;
}

function calculateFontSize($texto) {
    global $pdf;
	
	$maxWidth = 76;
	$fontFamily = "Arial";
	$fontSizeStart = 8;

    $fontSize = $fontSizeStart;

    do {
        $pdf->SetFont($fontFamily, '', $fontSize);
        $textWidth = $pdf->GetStringWidth($texto);

        if ($textWidth <= $maxWidth) {
            return $fontSize;
        }

        $fontSize -= 0.1;
    } while ($fontSize > 0);

    return $fontSizeStart; // Retornar tamanho de fonte padrão se não couber
}

function breakPhrase($phrase, $maxLength) {
    $words = explode(' ', $phrase); // Divide a frase em palavras
    $lines = array();
    $line = '';

    foreach ($words as $word) {
        // Verifica se a próxima palavra cabe na linha atual
        if (strlen($line) + strlen($word) + 1 <= $maxLength) {
            $line .= $word . ' ';
        } else {
            $lines[] = rtrim($line);
            $line = $word . ' ';
        }
    }

    $lines[] = rtrim($line); // Adiciona a última linha
    return $lines;
}

function printLine($texto){
	global $pdf;
	$texto = utf8_decode($texto);
	$largura = calculateFontSize($texto);
	if($largura > 5.5){
		$pdf->SetFont('Arial', '', $largura);
		$pdf->Text(2, novaLinha(), $texto);
		
	}else{
		$linhas = breakPhrase($texto, 65);
		foreach ($linhas as $line) {
			$largura = calculateFontSize($line);
			$pdf->SetFont('Arial', '', $largura);
			$pdf->Text(2, novaLinha(), $line);		
		}
	}

	
}

?>
