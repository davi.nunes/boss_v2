<!DOCTYPE html>
  <html>
    <head>
	<meta charset="UTF-8">
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
          <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">

    <link rel="stylesheet" href="meu.css?<?php echo time();?>">

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>

    <body>
	


	<main>
		<div class='container'>
		  <form class="col s12">
			<!-- Primeira linha de bot�es -->
			<div class="row">
			  <div class="input-field col s3">
				<label for="dataSelecionada">Selecione uma data:</label>
				<input type="date" id="dataSelecionada" class="validate">
			  </div>
			  <div class="input-field col s3 browser-default">
				  <select id="filtroStatus">
					<option value="1">Entrada</option>
					<option value="2">Prorroga��o</option>
					<option value="3">A Vencer</option>
					<option value="4">Vencido</option>
					<option value="5" selected>Liquida��o</option>
					<option value="6">Baixa</option>
				  </select>
				  <label for="filtroStatus">Filtrar por status:</label>
				</div>
			<div class="input-field col s4">
			  <a class='btn' id='movimento'>
				Consultar Movimenta��o do dia
			  </a>
			</div>			
			<div class="input-field col s2">
			  <a class='btn' id='movimentoPix'>
				Pix do Dia
			  </a>
			</div>
			  <div class="input-field col s3 hide">
				<a class='btn' id='consultaMovimento' movimento='6394069'>2 - Verifica Solicita��o</a>
			  </div> 
			  <div class="input-field col s3 hide">
				<a class='btn' id='getMovimento' movimento='6394069' arquivo='1372258'>3 - Visualizar</a>
			  </div>  
			  <div class="input-field col s2 hide">
				<a class='btn' id='getExtrato' >Extrato</a>
			  </div> 
			</div>

			<!-- Segunda linha de campos de entrada -->
			<div class="row">
			  <div class="input-field col s6">
				<input id="nomeCpfCnpj" placeholder="Pesquisar IXC com CPF/Nome do Cliente" type="text" class="validate">
				<label for="last_name">Pesquisar IXC com CPF/Nome do Cliente</label>
					<div class="input-loader">
						<i class="fas fa-spinner fa-spin"></i>
					</div>
			  </div>
			  <div class="input-field col s6">
				<input placeholder="Pesquisar direto no Sicoob: (linha digitavel ou NossoNumero)" id="via_sicoob" type="text" class="validate">
				<label for="first_name">Pesquisar direto no Sicoob: (linha digitavel ou NossoNumero)</label>
					<div class="input-loader">
						<i class="fas fa-spinner fa-spin"></i>
					</div>
			  </div>
			</div>
		  </form>	
		</div>

		<!-- Div onde os resultados ser�o exibidos -->
		<div id="resposta"></div>

	</main>
	
<!-- Modal para o temporizador -->
<div id="modalTemporizador" class="modal">
  <div class="modal-content">
    <div class="temporizador-wrapper">
	<div class="temporizador"></div>
	<div class="temporizador-texto">Aguarde...</div>
	</div>
    
  </div>
</div>

	
	<script>
	  // Obter a data de ontem
	  const ontem = new Date();
	  ontem.setDate(ontem.getDate() - 1);

	  // Formatar a data para o formato AAAA-MM-DD
	  const ontemFormatado = ontem.toISOString().split('T')[0];

	  // Definir o valor padr�o do seletor de data como ontem
	  document.getElementById('dataSelecionada').value = ontemFormatado;
	</script>



		<script src="https://code.jquery.com/jquery-3.7.0.js" integrity="sha256-JlqSTELeR4TLqP0OG9dxM7yDPqX1ox/HfgiSLBj8+kM=" crossorigin="anonymous"></script>
      <!--JavaScript at end of body for optimized loading-->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.6.0/jszip.min.js"></script>
      <script type="text/javascript" src="script.js?<?php echo time();?>" charset="UTF-8"></script>
    </body>
  </html>