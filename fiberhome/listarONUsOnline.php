<head>

  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Listar ONU Online/Offline Fiberhome</title>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.2/css/bulma.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
  <link rel="stylesheet" href="listafh.css">
  <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
</head>


<div class="col s12">
    <h4 class="header">Lista ONU</h4>
    <div class="card horizontal">

      <div class="card-stacked">
        <div class="card-content">
          <p>Selecione OLT, SLOT e PON e clique em Pesquisar</p>

<div class='row'>
	<div class="input-field col s6 m6">
	<select id='OLT' name='OLT'>
	  <option vlan="1000" value="172.21.2.2" selected>Sede</option>
	  <option vlan="3041" value="10.168.169.2">Incra 8</option>
	  <option vlan="3042" value="172.21.1.2">Vendinha</option>
	  <option vlan="3044" value="172.21.3.2">Monte Alto</option>
	  <option vlan="3046" value="10.169.66.10">Morada dos P�ssaros</option>
	  <option vlan="3048" value="10.169.67.2">Padre L�cio</option>
	  <option vlan="3050" value="10.169.65.2">Aguas Lindas</option>
	</select>
	<label>OLT</label>
	</div>

	<div class="input-field col s6 m1">
		
		<a id='btn' class="waves-effect waves-light btn-small orange right">Buscar</a>
		
		
	</div>
	
	
</div>

        </div>

      </div>
    </div>
  </div>

<div id='lista' class="row">
    <div class="col s12 m10 ">
      <div class="card-panel ">
        <span  class="white-text">Aguardando escolha.
        </span>
      </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

<script src="listarOnline.js?<?php echo time(); ?>"></script>