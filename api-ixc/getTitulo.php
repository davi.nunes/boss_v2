<?php
//echo "<pre>";
if(!isset($_GET['boleto'])){
	exit;
}

$boleto = $_GET['boleto'];

require('api.php');
$host = 'https://acessodf.net/webservice/v1';
$token = '6:e6a38134ca5be7e571282d7cca2e0eb9420b32b152aa701e6ad5127306535277';//token gerado no cadastro do usuario (verificar permissões)
$selfSigned = false; //true para certificado auto assinado
$api = new IXCsoft\WebserviceClient($host, $token, $selfSigned);
$params = array(
    'boletos' => $boleto, // id da fatura
    'juro' => 'N', // 'S'->SIM e 'N'->NÃO para cálculo de júro
    'multa' => 'N', // 'S'->SIM e 'N'->NÃO para cálculo de multa
    'atualiza_boleto' => 'N', // 'S'->SIM e 'N'->NÃO para atualizar o boleto
    'tipo_boleto' => 'arquivo', // tipo de método que será executado
    'base64' => 'S' // para retornar arquivo em formato base64 informe 'S', para retornar binário, informe 'N' ou não informe
);
// var_dump($params);
// echo '<br>';
$api->get('get_boleto',$params);
$retorno = $api->getRespostaConteudo(false);// false para json | true para array
// echo "<pre>";
// var_dump($retorno);

// Verifica se a resposta é válida
if ($retorno !== false) {
    // Configura os cabeçalhos HTTP para exibir o PDF
    header('Content-type: application/pdf');
    header('Content-Disposition: inline; filename="documento.pdf"'); // Você pode alterar o nome do arquivo, se desejar

    // Decodifica a string em base64 para obter o conteúdo do PDF
    $pdfContent = base64_decode($retorno);

    // Exibe o PDF na tela
    echo $pdfContent;
} else {
    echo 'Erro ao obter o PDF.';
}
?>