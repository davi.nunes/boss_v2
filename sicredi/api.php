<?php
	
	include "config.php";

	function getToken(){ //Gera um token para ustilizar as demais endpoints

		global $xApiKey;
		global $agencia;
		global $posto;
		global $beneficiario;
		global $username;
		global $codigoAcesso;	
	
		$url = 'https://api-parceiro.sicredi.com.br/auth/openapi/token';
		
		$ch = curl_init($url); // Inicializa uma nova sessão cURL
		
		// Define as opções da solicitação
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
				'x-api-key: ' . $xApiKey,
				'context: COBRANCA' // Coloque aspas em torno do valor do cabeçalho
		]);

		
		// Define os parâmetros no payload
		$data = [
			'username' => $username,
			'password' => $codigoAcesso,
			'scope' => 'cobranca',
			'grant_type' => 'password',
		];
		$dataString = http_build_query($data);
		// var_dump($dataString);

		// Define o corpo da solicitação
		curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

		// Executa a solicitação e obtém a resposta
		$response = curl_exec($ch);

		// Verifica se ocorreu algum erro na solicitação
		if(curl_errno($ch)) {
			echo 'Erro na requisição: ' . curl_error($ch);
		}

		// Decodifica a resposta JSON
		$responseData = json_decode($response, true);

		// Extrai o token da resposta
		$token = $responseData['access_token'];

		return $token;
		
	}
	
	function getNossoNumero($token, $nossoNumero){ //Pega um Boleto

		global $xApiKey;
		global $agencia;
		global $posto;
		global $beneficiario;
		global $username;
		global $codigoAcesso;	
	
		$url = 'https://api-parceiro.sicredi.com.br/cobranca/boleto/v1/boletos';

		// Dados enviados via GET
		$data = [
			'codigoBeneficiario' => $beneficiario,
			'nossoNumero' => $nossoNumero,
		];
		
		$url .= '?' . http_build_query($data);
		
		$ch = curl_init($url); // Inicializa uma nova sessão cURL
		
		// Define as opções da solicitação
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
				'x-api-key: ' . $xApiKey,
				'cooperativa: ' . $agencia,
				'posto: ' .$posto,
				'Authorization: bearer '.$token,
				'Content-Type: application/x-www-form-urlencoded'
		]);


		// Executa a solicitação e obtém a resposta
		$response = curl_exec($ch);

		// Verifica se ocorreu algum erro na solicitação
		if(curl_errno($ch)) {
			echo 'Erro na requisição: ' . curl_error($ch);
		}

		// Decodifica a resposta JSON
		$responseData = json_decode($response, true);

		// Extrai o token da resposta
		$token = $responseData['access_token'];

		return $response;
		
	}	
	
	function getLiquidados($token, $dia){ //Pega um Boleto

		global $xApiKey;
		global $agencia;
		global $posto;
		global $beneficiario;
		global $username;
		global $codigoAcesso;	
	
		$url = 'https://api-parceiro.sicredi.com.br/cobranca/boleto/v1/boletos/liquidados/dia';

		// Dados enviados via GET
		$params = [
			'codigoBeneficiario' => $beneficiario,
			'dia' => $dia,
		];
		
		$url .= '?' . http_build_query($params);
		
		$ch = curl_init($url); // Inicializa uma nova sessão cURL
		
		// Define as opções da solicitação
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
				'x-api-key: ' . $xApiKey,
				'cooperativa: ' . $agencia,
				'posto: ' .$posto,
				'Authorization: bearer '.$token,
				'Content-Type: application/x-www-form-urlencoded'
		]);


		// Executa a solicitação e obtém a resposta
		$response = curl_exec($ch);

		// Verifica se ocorreu algum erro na solicitação
		if(curl_errno($ch)) {
			echo 'Erro na requisição: ' . curl_error($ch);
		}

		// Decodifica a resposta JSON
		$responseData = json_decode($response, true);

		// Extrai o token da resposta
		$token = $responseData['access_token'];

		return $response;
		
	}

?>