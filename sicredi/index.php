<?php // SICREDI

include "api.php";
header("Content-Type: text/html; charset=UTF-8");


$token = getToken();
$nossoNumero = $_GET["nossoNumero"];

if($_GET["qrCode"]){
	$qr = getNossoNumero($token, $nossoNumero);
	$qrqd = json_decode($qr);
	echo $qrqd->codigoQrCode;
}elseif($_GET["boleto"]){
	$qr = getNossoNumero($token, $nossoNumero);
	echo formataJson($qr);
	
}else{

	echo formataJson(getLiquidados($token, '28/07/2023'));
}

function formataJson($jsonString) {
    $jsonObj = json_decode($jsonString);
    if ($jsonObj === null) {
        return "JSON inválido.";
    }
    return json_encode($jsonObj, JSON_PRETTY_PRINT);
}
?>